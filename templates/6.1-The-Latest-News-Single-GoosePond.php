<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">

	<div class="lazybg" data-src="../assets/dist/images/temp/hero/hero-news.jpg">
	</div><!-- .lazybg -->
	
</div><!-- .hero -->

<div class="body">
	
	<section>
		<div class="sw">
		
			<article>
			
				<div class="hgroup">
					<h1 class="title">This is a News Article</h1>
					<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
				</div>
		
				<div class="main-body">
				
					<div class="content">				
					
						<div class="breadcrumbs">
							<a href="#">The Latest</a>
							<a href="#">This is a News Article</a>
						</div><!-- .breadcrumbs -->
					
						<div class="article-body">
						
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis tellus mollis orci, sed rhoncus sapien nunc eget odio.
							</p>
								 
							<p>
								Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
							</p>
								 
							<p>
								Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
							</p>
								 
							<p>
								At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque 
							</p>
							
							<h2>H2 - Header 2</h2>
							<h3>H3 - Header 3</h3>
							<h4>H4 - Header 4</h4>
							<h5>H5 - Header 5</h5>
							<h6>H6 - Header 6</h6>
						
							<h2>Links</h2>
							
							<p>
								<a href="#link">Link</a> <br />
								<a href="#link" class="hover">Link (hovered)</a> <br />
								<a href="#link" class="visited">Link (visited)</a> <br />
							</p>
						
						</div><!-- .article-body -->
					</div><!-- .content -->
					<aside class="sidebar">
						
						<div class="archives-mod mod box">
							<div class="hgroup">
								<h4 class="title">Archives</h4>
							</div><!-- .hgroup -->
							
							<div class="acc with-indicators inv">
							
								<div class="acc-item">
									<div class="acc-item-handle">
										2014
									</div><!-- .acc-item-handle -->
									<div class="acc-item-content">
										<ul>
											<li><a href="#">October (2)</a></li>
											<li><a href="#">September (5)</a></li>
											<li><a href="#">August (6)</a></li>
										</ul>
									</div><!-- .acc-item-content -->
								</div><!-- .acc-item -->
								
								<div class="acc-item">
									<div class="acc-item-handle">
										2014
									</div><!-- .acc-item-handle -->
									<div class="acc-item-content">
										<ul>
											<li><a href="#">October (2)</a></li>
											<li><a href="#">September (5)</a></li>
											<li><a href="#">August (6)</a></li>
										</ul>
									</div><!-- .acc-item-content -->
								</div><!-- .acc-item -->
								
							</div><!-- .acc -->
							
						</div><!-- .archives-mod -->
						
					</aside><!-- .sidebar -->
				</div><!-- .main-body -->
			
			</article>
		
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>