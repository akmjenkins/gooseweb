<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">
	
	<section>
		<div class="sw">
		
			<div class="hgroup nopad">
				<h1 class="title">Goose Pond Properties</h1>
				<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
			</div>
		
		</div><!-- .sw -->
	</section>
	
	<section class="nopad">
		<div class="fader-wrap">
			<div class="big-fader fader">
				<div class="fader-item bgel" data-src="../assets/dist/images/temp/hero/hero-1.jpg">
				</div><!-- .fader-item -->
				<div class="fader-item bgel" data-src="../assets/dist/images/temp/hero/hero-2.jpg">
				</div><!-- .fader-item -->
			</div><!-- .fader -->
			
			<div class="fader-nav">&nbsp;</div>
			
		</div><!-- .fader-wrap -->
	</section><!-- .nopad -->
	
	<section class="grad">
		<div class="sw">
		
			<div class="grid vcenter pad40">
				
				<div class="col col-2 sm-col-1 sm-o-first">
					<div class="item">
					
						<div class="lazybg">
							<img src="../assets/dist/images/temp/home-1.jpg" alt="Calm lake">
						</div><!-- .lazybg -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col col-2 sm-col-1 o-first">
					<div class="item">
					
							
							<span class="h1-style">Get the view of your dreams.</span>
							<p>Phase 7 of Goose Pond Country Properties is now selling with Lakefront properties with views that will make your home feel like an everday vacation.</p>
							
							<a href="#" class="button">Find Out More</a>
							<a href="#" class="button">Contact Us</a>
							
					
					</div><!-- .item -->
				</div><!-- .col -->
			
			</div><!-- .grid -->
		
		</div><!-- .sw -->
	</section>
	
	<section class="dark">
		<div class="sw">
		
			<div class="hgroup d-bg">
				<h4 class="title">Featured Listings</h4>
				<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
			</div><!-- .hgroup -->
			
			<div class="grid eqh collapse-at-850 blocks">
				
				<div class="col-3 col">
					<div class="item box">
					
						<a class="block with-button" href="#">
						
							<span class="new-listing-banner">New Listing</span>
						
							<div class="img-wrap">
								<div class="img lazybg" data-src="../assets/dist/images/temp/lakefront-header.jpg"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<div class="hgroup">
									<span class="h4-style title">Listing Number One</span>
									<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
								</div>
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort.</p>
								
								<span class="button">More Info</span>
								
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col-3 col">
					<div class="item box">
					
						<a class="block with-button" href="#">
						
							<div class="img-wrap">
								<div class="img lazybg" data-src="../assets/dist/images/temp/lakefront-header.jpg"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<div class="hgroup">
									<span class="h4-style title">Listing Number One</span>
									<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
								</div>
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
								
								<span class="button">More Info</span>
								
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col-3 col">
					<div class="item box">
					
						<a class="block with-button" href="#">
						
							<div class="img-wrap">
								<div class="img lazybg" data-src="../assets/dist/images/temp/lakefront-header.jpg"></div>
							</div><!-- .img-wrap -->
							<div class="content">
							
								<div class="hgroup">
									<span class="h4-style title">Listing Number One</span>
									<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
								</div>
								
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
								
								<span class="button">More Info</span>
								
							</div><!-- .content -->
						</a><!-- .block -->
						
					</div><!-- .item -->
				</div><!-- .col -->

			</div><!-- .grid -->
			
			<div class="center">
				<button class="button blue">View More</button>
			</div><!-- .center -->
		
		
		</div><!-- .sw -->
	</section>

	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>