<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="lazybg" data-src="../assets/dist/images/temp/hero/hero-faq.jpg">
	</div><!-- .lazybg -->	
</div><!-- .hero -->

<div class="body">
	
	<section>
		<div class="sw">
		
			<article>
			
				<div class="hgroup">
					<h1 class="title">FAQ</h1>
					<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
				</div>
		
				<div class="main-body">
				
					<div class="content">				
					
						<div class="breadcrumbs">
							<a href="#">FAQ</a>
						</div><!-- .breadcrumbs -->
					
						<div class="article-body">
						
							<p>
								Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laoreet. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin
								sodales pulvinar tempor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Nam fermentum, nulla luctus pharetra vulputate, felis
								tellus mollis orci, sed rhoncus sapien nunc eget odio.
							</p>
							
						</div><!-- .article-body -->
						
						<div class="acc with-indicators separated">
						
							<div class="acc-item">
								<div class="acc-item-handle">
									Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
								</div>
								<div class="acc-item-content">
									<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi
									sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga</p>
								</div>
							</div>
							
							<div class="acc-item">
								<div class="acc-item-handle">
									Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
								</div>
								<div class="acc-item-content">
									<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi
									sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga</p>
								</div>
							</div>
							
							<div class="acc-item">
								<div class="acc-item-handle">
									Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
								</div>
								<div class="acc-item-content">
									<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi
									sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga</p>
								</div>
							</div>
							
							<div class="acc-item">
								<div class="acc-item-handle">
									Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur?
								</div>
								<div class="acc-item-content">
									<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi
									sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga</p>
								</div>
							</div>
							
						</div>
						
					</div><!-- .content -->
				</div><!-- .main-body -->
			
			</article>
		
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>