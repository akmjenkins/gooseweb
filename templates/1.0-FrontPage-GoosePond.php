<?php $bodyclass = 'home'; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" >
		
			<a href="#" class="clickable-image bgel fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-1.jpg">&nbsp;</a>
			
			<div class="hero-caption dark-bg sw full">
				<div class="hero-caption-title hc-block">
					Phase 7 of Goose Pond Development Now Selling!
				</div><!-- .hero-caption-title -->
				<div class="hero-caption-subtitle hc-block">
					Many great waterfront and non-waterfront lots to choose from.
				</div><!-- .hero-caption-subtitle -->
				<div class="hero-caption-button hc-block">
					<a href="#" class="button">Find Out More</a>
				</div><!-- .hero-caption-button -->
			</div><!-- .hero-caption -->

		
		</div><!-- .fader-item -->
		<div class="fader-item">
		
			<a href="#" class="clickable-image bgel fader-item-bg" data-src="../assets/dist/images/temp/hero/hero-2.jpg">&nbsp;</a>
		
			<div class="hero-caption dark-bg sw full">
				<div class="hero-caption-title hc-block">
					Caption #2 Phase 7 of Goose Pond Development
				</div><!-- .hero-caption-title -->
				<div class="hero-caption-subtitle hc-block">
					Caption #2 Many great waterfront and non-waterfront lots to choose from.
				</div><!-- .hero-caption-subtitle -->
				<div class="hero-caption-button hc-block">
					<a href="#" class="button">Find Out More</a>
				</div><!-- .hero-caption-button -->
			</div><!-- .hero-caption -->
		
		</div><!-- .fader-item -->
		
	</div><!-- .fader -->
	
	<div class="fader-nav">&nbsp;</div>
	
</div><!-- .hero -->

<div class="body">
	
	<section>
		<div class="sw">
		
			<div class="grid vcenter pad40">
				
				<div class="col col-2 sm-col-1">
					<div class="item">
					
						<div class="lazybg">
							<img src="../assets/dist/images/temp/home-1.jpg" alt="Calm lake">
						</div><!-- .lazybg -->
						
					</div><!-- .item -->
				</div><!-- .col -->
				
				<div class="col col-2 sm-col-1">
					<div class="item">
					
							
							<span class="h1-style">Get the view of your dreams.</span>
							<p>Phase 7 of Goose Pond Country Properties is now selling with Lakefront properties with views that will make your home feel like an everday vacation.</p>
							
							<a href="#" class="button">Find Out More</a>
							<a href="#" class="button">Contact Us</a>
							
					
					</div><!-- .item -->
				</div><!-- .col -->
			
			</div><!-- .grid -->
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>