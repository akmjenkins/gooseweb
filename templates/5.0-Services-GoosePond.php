<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="fader-wrap">
		<div class="little-fader fader">
			<div class="fader-item bgel" data-src="../assets/dist/images/temp/hero/hero-tree.jpg">
			</div><!-- .fader-item -->
			<div class="fader-item bgel" data-src="../assets/dist/images/temp/hero/hero-2.jpg">
			</div><!-- .fader-item -->
		</div><!-- .fader -->
	</div><!-- .fader-wrap -->
</div><!-- .hero -->

<div class="body">
	
	<article>
	
		<section>
			<div class="sw">
			
				<div class="hgroup">
					<h1 class="title">Services</h1>
					<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
				</div><!-- .hgroup -->
						
				<div class="breadcrumbs">
					<a href="#">Projects</a>
					<a href="#">Services</a>
				</div><!-- .breadcrumbs -->
				
				<hr />
				
			</div><!-- .sw -->
		</section>
		
		<div class="article-body">
		
			<section class="img-section">
				
				<div class="content">
			
					<div class="hgroup">
						<h4 class="title">Heavy Equipment Rentals</h4>
						<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
					</div><!-- .hgroup -->
					
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum 
						laoreet. Proin gravida dolor sit amet lacusaccumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. euismod bibendum laoreet. Proin gravida dolor 
						sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor
					</p>
				
				</div><!-- .content -->
				
				<div class="img-wrap rounded no-sm">
					<div class="lazybg" data-src="../assets/dist/images/temp/heavy-equipment.png"></div>
				</div>
					
			</section>
			
			<!-- wrapping in a no-sm class conserves bandwidth for unnecessary embellishments on mobile devices -->
			<div class="no-sm">
				<div class="inline-img lazybg" data-src="../assets/dist/images/temp/body-image-2.jpg">
					&nbsp;
				</div>
			</div><!-- .no-sm -->
			
			<section class="img-section">
				
				<div class="content">
			
					<div class="hgroup">
						<h4 class="title">Lot Clearing &amp; Leveling</h4>
						<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
					</div><!-- .hgroup -->
					
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum 
						laoreet. Proin gravida dolor sit amet lacusaccumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. euismod bibendum laoreet. Proin gravida dolor 
						sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor
					</p>
				
				</div><!-- .content -->
				
				<div class="img-wrap rounded no-sm">
					<div class="lazybg" data-src="../assets/dist/images/temp/lot-clearing.png"></div>
				</div>
					
			</section>
			
			<div class="no-sm">
				<div class="inline-img lazybg" data-src="../assets/dist/images/temp/body-image-3.jpg">
					&nbsp;
				</div>
			</div><!-- .no-sm -->
			
			<section class="img-section">
				
				<div class="content">
			
					<div class="hgroup">
						<h4 class="title">Driveway Installation</h4>
						<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
					</div><!-- .hgroup -->
					
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum 
						laoreet. Proin gravida dolor sit amet lacusaccumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. euismod bibendum laoreet. Proin gravida dolor 
						sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor
					</p>
				
				</div><!-- .content -->
				
				<div class="img-wrap rounded no-sm">
					<div class="lazybg" data-src="../assets/dist/images/temp/driveway.png"></div>
				</div>
					
			</section>

		</div><!-- .article-body -->
	
	</article>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>