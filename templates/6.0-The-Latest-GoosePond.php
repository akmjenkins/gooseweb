<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">

	<div class="lazybg" data-src="../assets/dist/images/temp/hero/hero-news.jpg">
	</div><!-- .lazybg -->
	
</div><!-- .hero -->

<div class="body">
	
	<section>
		<div class="sw">
			
			<div class="hgroup nopad">
				<h1 class="title">The Latest</h1>
				<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
			</div>
		
		</div><!-- .sw -->
		
	</section>
	
	<section>
	
		<div class="filter-section">
		
			<div class="filter-bar">
				<div class="sw">
				
					<div class="filter-bar-left">
						<div class="count">						
							<span class="title">News</span>
							6 News Articles
						</div><!-- .count -->
					</div><!-- .filter-bar-left -->
				
					<div class="filter-bar-meta">
					
						<div class="filter-controls">
							<button class="previous">Prev</button>
							<button class="next">Next</button>
						</div><!-- .filter-controls -->
					
					</div><!-- .filter-bar-meta -->
					
				</div><!-- .sw -->
			</div><!-- .filter-bar -->
			
			<div class="filter-content">
				<div class="sw">
				
					<div class="grid eqh blocks collapse-at-650 blocks">
						
						<div class="col-3 col sm-col-1">
							<div class="item box">
							
								<a class="block with-button" href="#">
								
									<div class="img-wrap">
										<div class="img lazybg" data-src="../assets/dist/images/temp/block-1.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="hgroup">
											<span class="h4-style title">This is a News Article</span>
											<span class="subtitle">Sub title</span>
										</div>
										
										<time datetime="2014-10-14">Oct 14, 2014</time>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
										
										<span class="button">More Info</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->

						<div class="col-3 col sm-col-1">
							<div class="item box">
							
								<a class="block with-button" href="#">
								
									<div class="img-wrap">
										<div class="img lazybg" data-src="../assets/dist/images/temp/block-1.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="hgroup">
											<span class="h4-style title">This is a News Article</span>
											<span class="subtitle">Sub title</span>
										</div>
										
										<time datetime="2014-10-14">Oct 14, 2014</time>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort.</p>
										
										<span class="button">More Info</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-1">
							<div class="item box">
							
								<a class="block with-button" href="#">
								
									<div class="img-wrap">
										<div class="img lazybg" data-src="../assets/dist/images/temp/block-1.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="hgroup">
											<span class="h4-style title">This is a News Article</span>
											<span class="subtitle">Sub title</span>
										</div>
										
										<time datetime="2014-10-14">Oct 14, 2014</time>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
										
										<span class="button">More Info</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->

						
					</div><!-- .grid -->
					
				</div><!-- .sw -->
			</div><!-- .filter-content -->
			
		</div><!-- .filter-section -->
	
	</section>
	
	<section>
	
		<div class="filter-section">
		
			<div class="filter-bar">
				<div class="sw">
				
					<div class="filter-bar-left">
						<div class="count">
							<span class="title">Testimonials</span>
							10 Testimonials
						</div><!-- .count -->
					</div><!-- .filter-bar-left -->
				
					<div class="filter-bar-meta">
					
						<div class="filter-controls">
							<button class="previous">Prev</button>
							<button class="next">Next</button>
						</div><!-- .filter-controls -->
					
					</div><!-- .filter-bar-meta -->
					
				</div><!-- .sw -->
			</div><!-- .filter-bar -->
			
			<div class="filter-content">
				<div class="sw">
				
					<div class="grid">
						<div class="col-2 col sm-col-1">
							<div class="item">
							
								<div class="testimonial">
									
									<div class="img-wrap rounded">
										<div class="lazybg" data-src="../assets/dist/images/temp/face-1.png"></div>
									</div><!-- .img-wrap -->
									
									<blockquote>
									
										"Neque porro quisquam est, qui dolorem ipsum
										quia dolor sit amet, consectetur, adipisci velit, sed
										quia non numquam eius modi tempora incidunt ut
										labore et dolore magnam aliquam quaerat
										voluptatem."
									
										<cite>Happy Customer</cite>
									</blockquote>
									
								</div><!-- .testimonial -->
							
							</div><!-- .item -->
						</div><!-- .col-2 -->
						<div class="col-2 col sm-col-1">
							<div class="item">
							
								<div class="testimonial">
									
									<div class="img-wrap rounded">
										<div class="lazybg" data-src="../assets/dist/images/temp/face-1.png"></div>
									</div><!-- .img-wrap -->
									
									<blockquote>
									
										"Neque porro quisquam est, qui dolorem ipsum
										quia dolor sit amet, consectetur, adipisci velit, sed
										quia non numquam eius modi tempora incidunt ut"
									
										<cite>Happy Customer</cite>
									</blockquote>
									
								</div><!-- .testimonial -->
							
							</div><!-- .item -->
						</div><!-- .col-2 -->
						<div class="col-2 col sm-col-1">
							<div class="item">
							
								<div class="testimonial">
									
									<div class="img-wrap rounded">
										<div class="lazybg" data-src="../assets/dist/images/temp/face-1.png"></div>
									</div><!-- .img-wrap -->
									
									<blockquote>
									
										"Neque porro quisquam est, qui dolorem ipsum
										quia dolor sit amet, consectetur, adipisci velit, sed
										quia non numquam"
									
										<cite>Happy Customer</cite>
									</blockquote>
									
								</div><!-- .testimonial -->
							
							</div><!-- .item -->
						</div><!-- .col-2 -->
						<div class="col-2 col sm-col-1">
							<div class="item">
							
								<div class="testimonial">
									
									<div class="img-wrap rounded">
										<div class="lazybg" data-src="../assets/dist/images/temp/face-1.png"></div>
									</div><!-- .img-wrap -->
									
									<blockquote>
									
										"Neque porro quisquam est, qui dolorem ipsum
										quia dolor sit"
									
										<cite>Happy Customer</cite>
									</blockquote>
									
								</div><!-- .testimonial -->
							
							</div><!-- .item -->
						</div><!-- .col-2 -->

					</div><!-- .grid -->
				
					
				</div><!-- .sw -->
			</div><!-- .filter-content -->
			
		</div><!-- .filter-section -->
	
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>