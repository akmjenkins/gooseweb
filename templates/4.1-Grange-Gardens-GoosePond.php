<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="fader-wrap">
		<div class="little-fader fader">
			<div class="fader-item bgel" data-src="../assets/dist/images/temp/hero/hero-tree.jpg">
			</div><!-- .fader-item -->
			<div class="fader-item bgel" data-src="../assets/dist/images/temp/hero/hero-2.jpg">
			</div><!-- .fader-item -->
		</div><!-- .fader -->
		<div class="fader-nav">&nbsp;</div>
	</div><!-- .fader-wrap -->
</div><!-- .hero -->

<div class="body">
	
	<article>
	
		<section>
			<div class="sw full">
			
				<div class="hgroup">
					<h1 class="title">Grange Gardens</h1>
					<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
				</div><!-- .hgroup -->
				
			</div><!-- .sw -->
			
			<div class="bar-nav">
				<div class="sw">
					<span>In This Section</span>
					<a href="#" class="selected">Grange Gardens</a>
					<a href="#">Custom Lot Designs</a>
				</div><!-- .sw -->
			</div><!-- .bar-nav -->
			
		</section>
		
		<div class="article-body">
		
			<section class="img-section">
				
				<div class="content">
			
					<div class="hgroup">
						<h4 class="title">Feature 1</h4>
						<span class="subtitle">Sub Title</span>
					</div><!-- .hgroup -->
					
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum 
						laoreet. Proin gravida dolor sit amet lacusaccumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. euismod bibendum laoreet. Proin gravida dolor 
						sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor
					</p>
				
				</div><!-- .content -->
				
				<div class="img-wrap rounded no-sm">
					<div class="lazybg" data-src="../assets/dist/images/temp/feature.png"></div>
				</div>
					
			</section>
			
			<!-- wrapping in a no-sm class conserves bandwidth for unnecessary embellishments on mobile devices -->
			<div class="no-sm">
				<div class="inline-img lazybg" data-src="../assets/dist/images/temp/body-image-1.jpg">
					&nbsp;
				</div>
			</div><!-- .no-sm -->
			
			<section class="img-section">
				
				<div class="content">
			
					<div class="hgroup">
						<h4 class="title">Feature 2</h4>
						<span class="subtitle">Sub Title</span>
					</div><!-- .hgroup -->
					
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum 
						laoreet. Proin gravida dolor sit amet lacusaccumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. euismod bibendum laoreet. Proin gravida dolor 
						sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor
					</p>
				
				</div><!-- .content -->
				
				<div class="img-wrap rounded no-sm">
					<div class="lazybg" data-src="../assets/dist/images/temp/feature.png"></div>
				</div>
					
			</section>
			
			<div class="no-sm">
				<div class="inline-img lazybg" data-src="../assets/dist/images/temp/body-image-1.jpg">
					&nbsp;
				</div>
			</div><!-- .no-sm -->
			
			<section class="img-section">
				
				<div class="content">
			
					<div class="hgroup">
						<h4 class="title">Feature 3</h4>
						<span class="subtitle">Sub Title</span>
					</div><!-- .hgroup -->
					
					<p>
						Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum 
						laoreet. Proin gravida dolor sit amet lacusaccumsan et viverra justo commodo. 
						Proin sodales pulvinar tempor. euismod bibendum laoreet. Proin gravida dolor 
						sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar tempor
					</p>
				
				</div><!-- .content -->
				
				<div class="img-wrap rounded no-sm">
					<div class="lazybg" data-src="../assets/dist/images/temp/feature.png"></div>
				</div>
					
			</section>

		</div><!-- .article-body -->
	
	</article>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>