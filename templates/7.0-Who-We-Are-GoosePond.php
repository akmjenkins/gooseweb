<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">
	<div class="lazybg" data-src="../assets/dist/images/temp/hero/hero-who-we-are.jpg">
	</div><!-- .lazybg -->	
</div><!-- .hero -->

<div class="body">
	
	<section>
		<div class="sw">
		
			<div class="hgroup">
				<h1 class="title">Who We Are</h1>
				<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
			</div><!-- .hgroup -->
		
			<div class="grid eqh pad40">
				
				<div class="col col-3 sm-col-1">
					<a class="item overview-half-block bounce" href="#">
						
						<div class="img-wrap rounded">
							<div class="lazybg img" data-src="../assets/dist/images/temp/gooseweb-circle.png">
							</div><!-- .lazybg -->
						</div><!-- .img-wrap -->
						
						<div class="hgroup">
							<h4 class="title">Our Company</h4>
							<span class="subtitle">Sub Title</span>
						</div>
						
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean 
						</p>
						
						<div class="btn">
							<span class="button">Read More</span>
						</div><!-- .btn -->
					
					</a><!-- .item -->
				</div><!-- .col -->
				
				<div class="col col-3 sm-col-1">
					<a class="item overview-half-block bounce" href="#">
						
						<div class="img-wrap rounded">
							<div class="lazybg img" data-src="../assets/dist/images/temp/our-experience.png">
							</div><!-- .lazybg -->
						</div><!-- .img-wrap -->
						
						<div class="hgroup">
							<h4 class="title">Our Experience</h4>
							<span class="subtitle">Sub Title</span>
						</div>
						
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
							euismod bibendum laoreet. Proin gravida dolor sit amet lacus
							accumsan et viverra justo commodo. Proin sodales pulvinar tempor.
						</p>
						
						<div class="btn">
							<span class="button">Read More</span>
						</div><!-- .btn -->
					
					</a><!-- .item -->
				</div><!-- .col -->

				<div class="col col-3 sm-col-1">
					<a class="item overview-half-block bounce" href="#">
						
						<div class="img-wrap rounded">
							<div class="lazybg img" data-src="../assets/dist/images/temp/testimonials.png">
							</div><!-- .lazybg -->
						</div><!-- .img-wrap -->
						
						<div class="hgroup">
							<h4 class="title">Testimonials</h4>
							<span class="subtitle">Sub Title</span>
						</div>
						
						<p>
							Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean
							euismod bibendum laoreet. Proin gravida dolor sit amet lacus
							accumsan et viverra justo commodo. Proin sodales pulvinar tempor.
						</p>
						
						<div class="btn">
							<span class="button">Read More</span>
						</div><!-- .btn -->
					
					</a><!-- .item -->
				</div><!-- .col -->
				
			</div><!-- .grid -->
		
		</div><!-- .sw -->
	</section>

	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>