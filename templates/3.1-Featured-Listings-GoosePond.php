<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="body">
	
	<section>
		<div class="sw">
		
			<div class="hgroup">
				<h1 class="title">Goose Pond Properties</h1>
				<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
			</div>
		
		</div><!-- .sw -->
	
		<div class="filter-section">
			<div class="filter-bar">
				<div class="sw">

					<div class="filter-bar-left">
					
						<div class="count">
							6 Listings Found
						</div><!-- .count -->
						
						<div class="filter-bar-buttons">
							<button class="button">New Listings</button>
							
							<div class="selector with-arrow">
								<select name="location">
									<option value="">Location</option>
									<option value="1" data-tag="Goose Pond">Goose Pond</option>
									<option value="2" data-tag="Placentia">Placentia</option>
									<option value="3" data-tag="Cow Head">Cow Head</option>
								</select>
								<span class="value">&nbsp;</span>
							</div><!-- .selector -->
							
						</div><!-- .filter-bar-buttons -->
						
					</div><!-- .filter-bar-left -->

					<div class="filter-bar-meta">
					
						<form action="/" method="post" class="search-form single-form">
							<fieldset>
								<input type="text" name="s" placeholder="Search listings...">
								<button class="t-fa-abs fa-search">Search Listings</button>
							</fieldset>
						</form>
						
						<div class="filter-controls">
							<button class="previous">Prev</button>
							<button class="next">Next</button>
						</div><!-- .filter-controls -->
					
					</div><!-- .filter-bar-meta -->
					
				</div><!-- .sw -->
			</div><!-- .filter-bar -->
			
			<div class="filter-content">
			
				<div class="sw">
					
					<div class="grid eqh blocks collapse-at-850 blocks">
						
						<div class="col-3 col sm-col-2">
							<div class="item box">
							
								<a class="block with-button keep-img " href="#">
								
									<span class="new-listing-banner">New Listing</span>
								
									<div class="img-wrap">
										<div class="img lazybg" data-src="../assets/dist/images/temp/lakefront-header.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="hgroup">
											<span class="h4-style title">Listing Number One</span>
											<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
										
										<span class="button">More Info</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-2">
							<div class="item box">
							
								<a class="block with-button keep-img" href="#">
								
									<div class="img-wrap">
										<div class="img lazybg" data-src="../assets/dist/images/temp/lakefront-header.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="hgroup">
											<span class="h4-style title">Listing Number One</span>
											<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
										
										<span class="button">More Info</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-2">
							<div class="item box">
							
								<a class="block with-button keep-img" href="#">
								
									<div class="img-wrap">
										<div class="img lazybg" data-src="../assets/dist/images/temp/lakefront-header.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="hgroup">
											<span class="h4-style title">Listing Number One</span>
											<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
										
										<span class="button">More Info</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->

						
						<div class="col-3 col sm-col-2">
							<div class="item box">
							
								<a class="block with-button keep-img" href="#">
								
									<span class="new-listing-banner">New Listing</span>
								
									<div class="img-wrap">
										<div class="img lazybg" data-src="../assets/dist/images/temp/lakefront-header.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="hgroup">
											<span class="h4-style title">Listing Number One</span>
											<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
										
										<span class="button">More Info</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-2">
							<div class="item box">
							
								<a class="block with-button keep-img" href="#">
								
									<div class="img-wrap">
										<div class="img lazybg" data-src="../assets/dist/images/temp/lakefront-header.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="hgroup">
											<span class="h4-style title">Listing Number One</span>
											<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
										
										<span class="button">More Info</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
						<div class="col-3 col sm-col-2">
							<div class="item box">
							
								<a class="block with-button keep-img" href="#">
								
									<div class="img-wrap">
										<div class="img lazybg" data-src="../assets/dist/images/temp/lakefront-header.jpg"></div>
									</div><!-- .img-wrap -->
									<div class="content">
									
										<div class="hgroup">
											<span class="h4-style title">Listing Number One</span>
											<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
										</div>
										
										<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean euismod bibendum laort. Proin gravida dolor sit amet lacus accumsan et viverra justo commodo. Proin sodales pulvinar</p>
										
										<span class="button">More Info</span>
										
									</div><!-- .content -->
								</a><!-- .block -->
								
							</div><!-- .item -->
						</div><!-- .col -->
						
					</div><!-- .grid -->
				
				</div><!-- .sw -->

				
			</div>
		</div>
	
	</section>

	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>