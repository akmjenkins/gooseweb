<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero">

	<div class="lazybg" data-src="../assets/dist/images/temp/hero/hero-contact.jpg">
	</div><!-- .lazybg -->
	
</div><!-- .hero -->

<div class="body">
	
	<section>
		<div class="sw">
		
			<article>
			
				<div class="hgroup">
					<h1 class="title">Contact Us</h1>
					<span class="subtitle">Lorem Ipsum Dolor sit Amet Consectetur</span>
				</div>
		
				<div class="main-body">
				
					<div class="content">				
					
						<div class="breadcrumbs">
							<a href="#">Contact Us</a>
						</div><!-- .breadcrumbs -->
					
						<div class="article-body">
						
							<div class="grid">
								<div class="col col-2 sm-col-1">
									<div class="item">
									
										<div class="hgroup">
											<h5 class="title uc">Goose Developments</h5>
										</div>
										
										<div class="grid">
										
											<div class="col col-2 xs-col-1">
											
												<div class="item">
													<address>
														123 This Street <br />
														Whitbourne, NL <br />
														A1B 2C3
													</address>
												</div><!-- .item -->
												
											</div><!-- .col -->
											
											<div class="col col-2 xs-col-1">
											
												<div class="item">
													
													<div class="rows">
														<div class="row">
															<span class="l">Local:</span>
															<span class="r">709 722 5741</span>
														</div>
														<div class="row">
															<span class="l">Toll Free:</span>
															<span class="r">800 285 8870</span>
														</div>
														<div class="row">
															<span class="l">After Hours:</span>
															<span class="r">709 834 4190</span>
														</div>
													</div><!-- .rows -->
													
												</div><!-- .item -->
											
											</div><!-- .col -->
											
										</div><!-- .grid -->
										
										<div class="hgroup">
											<h5 class="title uc">Hours of Operation</h5>
										</div>
										
										<span class="block">Monday - Friday &mdash; 9:00am - 5:00pm</span>
										<span class="block">Saturday &mdash; 9:00am - 5:00pm</span>
										<span class="block">Sunday &mdash; Closed</span>
										
									</div><!-- .item -->
								</div><!-- .col -->
								<div class="col col-2 sm-col-1">
									<div class="item">
										
										<form action="/" class="body-form contact-form">
											<fieldset>
											
												<input type="text" name="name" placeholder="Name">
												<input type="email" name="email" placeholder="Email">
												<textarea cols="30" rows="10" placeholder="Message"></textarea>
											
												<button class="button" type="submit">Submit</button>
											</fieldset>
										</form>
										
									</div>
								</div>
								<div class="col col-1 o-first sm-o-last">
									<div class="gmap">
										<div class="map"
											data-zoom="16"
											data-center="47.4335101,-53.5151935"
											data-markers='[{"title":"Goose Pond","position":"47.4335101,-53.5151935","image":"../assets/dist/images/goose-pond-map-marker-shadow.png","anchor":{"x":54,"y":135}}]'
										></div>
									</div>
								</div>
							</div><!-- .grid -->
							
							
							
						</div><!-- .article-body -->
						
					</div><!-- .content -->
				</div><!-- .main-body -->
			
			</article>
		
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>