<?php

	$lots = array(
		'1' => array(
			'title' => 'Lot 1',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.419607,'lng'=>-53.523245),
				array('lat'=>47.419627,'lng'=>-53.522204),
				array('lat'=>47.419995,'lng'=>-53.522167),
				array('lat'=>47.420016,'lng'=>-53.523193)
			)
		),
		'2' => array(
			'title' => 'Lot 2',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.420016,'lng'=>-53.523193),
				array('lat'=>47.419995,'lng'=>-53.522167),
				array('lat'=>47.420356,'lng'=>-53.522132),
				array('lat'=>47.420383,'lng'=>-53.523147)
			)
		),
		'3' => array(
			'title' => 'Lot 3',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.420383,'lng'=>-53.523147),
				array('lat'=>47.420356,'lng'=>-53.522132),
				array('lat'=>47.420722,'lng'=>-53.522094),
				array('lat'=>47.420742,'lng'=>-53.523101)
			)
		),
		'4' => array(
			'title' => 'Lot 4',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.420742,'lng'=>-53.523101),
				array('lat'=>47.420722,'lng'=>-53.522094),
				array('lat'=>47.421085,'lng'=>-53.522058),
				array('lat'=>47.421103,'lng'=>-53.523056)
			)
		),
		'5' => array(
			'title' => 'Lot 5',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.421103,'lng'=>-53.523056),
				array('lat'=>47.421085,'lng'=>-53.522058),
				array('lat'=>47.421445,'lng'=>-53.522021),
				array('lat'=>47.421466,'lng'=>-53.523007)
			)
		),
		'6' => array(
			'title' => 'Lot 6',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.421466,'lng'=>-53.523007),
				array('lat'=>47.421445,'lng'=>-53.522021),
				array('lat'=>47.421807,'lng'=>-53.521977),
				array('lat'=>47.421827,'lng'=>-53.52296)
			)
		),
		'7' => array(
			'title' => 'Lot 7',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.421827,'lng'=>-53.52296),
				array('lat'=>47.421807,'lng'=>-53.521977),
				array('lat'=>47.422169,'lng'=>-53.521929),
				array('lat'=>47.422192,'lng'=>-53.522916)
			)
		),
		'8' => array(
			'title' => 'Lot 8',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.422192,'lng'=>-53.522916),
				array('lat'=>47.422169,'lng'=>-53.521929),
				array('lat'=>47.422394,'lng'=>-53.521884),
				array('lat'=>47.422571,'lng'=>-53.521829),
				array('lat'=>47.422499,'lng'=>-53.522879)
			)
		),
		'9' => array(
			'title' => 'Lot 9',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.422499,'lng'=>-53.522879),
				array('lat'=>47.422571,'lng'=>-53.521829),
				array('lat'=>47.422769,'lng'=>-53.521724),
				array('lat'=>47.422926,'lng'=>-53.522022),
				array('lat'=>47.422882,'lng'=>-53.522829)
			)
		),
		'10' => array(
			'title' => 'Lot 10',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.422882,'lng'=>-53.522829),
				array('lat'=>47.422926,'lng'=>-53.522022),
				array('lat'=>47.423166,'lng'=>-53.522311),
				array('lat'=>47.423638,'lng'=>-53.52273)
			)
		),
		'11' => array(
			'title' => 'Lot 11',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.423637,'lng'=>-53.522729),
				array('lat'=>47.423612,'lng'=>-53.522578),
				array('lat'=>47.424211,'lng'=>-53.522059),
				array('lat'=>47.424466,'lng'=>-53.522499)
			)
		),
		'12' => array(
			'title' => 'Lot 12',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.423612,'lng'=>-53.522578),
				array('lat'=>47.423459,'lng'=>-53.522491),
				array('lat'=>47.423287,'lng'=>-53.522331),
				array('lat'=>47.42381,'lng'=>-53.521852),
				array('lat'=>47.424211,'lng'=>-53.522059)
			)
		),
		'13' => array(
			'title' => 'Lot 13',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.423287,'lng'=>-53.522331),
				array('lat'=>47.423163,'lng'=>-53.522197),
				array('lat'=>47.423006,'lng'=>-53.522003),
				array('lat'=>47.42354,'lng'=>-53.521488),
				array('lat'=>47.42367,'lng'=>-53.521754),
				array('lat'=>47.42381,'lng'=>-53.521852)
			)
		),
		'14' => array(
			'title' => 'Lot 14',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.423006,'lng'=>-53.522003),
				array('lat'=>47.422949,'lng'=>-53.521927),
				array('lat'=>47.42284,'lng'=>-53.521744),
				array('lat'=>47.422778,'lng'=>-53.521548),
				array('lat'=>47.423384,'lng'=>-53.521137),
				array('lat'=>47.42354,'lng'=>-53.521488)
			)
		),
		'15' => array(
			'title' => 'Lot 15',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.422778,'lng'=>-53.521548),
				array('lat'=>47.422673,'lng'=>-53.520752),
				array('lat'=>47.423335,'lng'=>-53.520674),
				array('lat'=>47.423384,'lng'=>-53.521137)
			)
		),
		'16' => array(
			'title' => 'Lot 16',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.422673,'lng'=>-53.520752),
				array('lat'=>47.423335,'lng'=>-53.520674),
				array('lat'=>47.423239,'lng'=>-53.520132),
				array('lat'=>47.422601,'lng'=>-53.520301)
			)
		),
		'17' => array(
			'title' => 'Lot 17',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.422601,'lng'=>-53.520301),
				array('lat'=>47.423239,'lng'=>-53.520132),
				array('lat'=>47.423053,'lng'=>-53.519678),
				array('lat'=>47.422535,'lng'=>-53.519815)
			)
		),
		'18' => array(
			'title' => 'Lot 18',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.422535,'lng'=>-53.519815),
				array('lat'=>47.423053,'lng'=>-53.519678),
				array('lat'=>47.423004,'lng'=>-53.519113),
				array('lat'=>47.422482,'lng'=>-53.519247)
			)
		),
		'19' => array(
			'title' => 'Lot 19',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.422482,'lng'=>-53.519247),
				array('lat'=>47.423004,'lng'=>-53.519113),
				array('lat'=>47.422917,'lng'=>-53.518467),
				array('lat'=>47.422392,'lng'=>-53.518652)
			)
		),
		'20' => array(
			'title' => 'Lot 20',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.422392,'lng'=>-53.518652),
				array('lat'=>47.422917,'lng'=>-53.518467),
				array('lat'=>47.422823,'lng'=>-53.517825),
				array('lat'=>47.422215,'lng'=>-53.518037)
			)
		),
		'21' => array(
			'title' => 'Lot 21',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.422455,'lng'=>-53.521783),
				array('lat'=>47.422568,'lng'=>-53.521745),
				array('lat'=>47.422747,'lng'=>-53.521649),
				array('lat'=>47.422634,'lng'=>-53.520817),
				array('lat'=>47.422527,'lng'=>-53.520179),
				array('lat'=>47.42236,'lng'=>-53.519763),
				array('lat'=>47.422288,'lng'=>-53.519983),
				array('lat'=>47.42215,'lng'=>-53.520112)
			)
		),
		'22' => array(
			'title' => 'Lot 22',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.42215,'lng'=>-53.520112),
				array('lat'=>47.421921,'lng'=>-53.520289),
				array('lat'=>47.422056,'lng'=>-53.521869),
				array('lat'=>47.422177,'lng'=>-53.521855),
				array('lat'=>47.422455,'lng'=>-53.521783)
			)
		),
		'23' => array(
			'title' => 'Lot 23',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.422056,'lng'=>-53.521869),
				array('lat'=>47.421921,'lng'=>-53.520289),
				array('lat'=>47.421703,'lng'=>-53.520284),
				array('lat'=>47.421671,'lng'=>-53.521913)
			)
		),
		'24' => array(
			'title' => 'Lot 24',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.421671,'lng'=>-53.521913),
				array('lat'=>47.421703,'lng'=>-53.520284),
				array('lat'=>47.421475,'lng'=>-53.520278),
				array('lat'=>47.421297,'lng'=>-53.521953)
			)
		),
		'25' => array(
			'title' => 'Lot 25',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.421297,'lng'=>-53.521953),
				array('lat'=>47.421475,'lng'=>-53.520278),
				array('lat'=>47.421192,'lng'=>-53.520445),
				array('lat'=>47.420878,'lng'=>-53.521998)
			)
		),
		'26' => array(
			'title' => 'Lot 26',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.420878,'lng'=>-53.521998),
				array('lat'=>47.421192,'lng'=>-53.520445),
				array('lat'=>47.42101,'lng'=>-53.520193),
				array('lat'=>47.420469,'lng'=>-53.522038)
			)
		),
		'27' => array(
			'title' => 'Lot 27',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.420469,'lng'=>-53.522038),
				array('lat'=>47.419712,'lng'=>-53.522105),
				array('lat'=>47.420814,'lng'=>-53.519897),
				array('lat'=>47.42101,'lng'=>-53.520193)
			)
		),
		'28' => array(
			'title' => 'Lot 28',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.419635,'lng'=>-53.522113),
				array('lat'=>47.420761,'lng'=>-53.519958),
				array('lat'=>47.420589,'lng'=>-53.519769),
				array('lat'=>47.419653,'lng'=>-53.521029)
			)
		),
		'29' => array(
			'title' => 'Lot 29',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.419653,'lng'=>-53.521029),
				array('lat'=>47.420589,'lng'=>-53.519769),
				array('lat'=>47.420502,'lng'=>-53.519645),
				array('lat'=>47.419671,'lng'=>-53.520262)
			)
		),
		'30' => array(
			'title' => 'Lot 30',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.419671,'lng'=>-53.520262),
				array('lat'=>47.420502,'lng'=>-53.519645),
				array('lat'=>47.420458,'lng'=>-53.519399),
				array('lat'=>47.419692,'lng'=>-53.51956)
			)
		),
		'31' => array(
			'title' => 'Lot 31',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.419692,'lng'=>-53.51956),
				array('lat'=>47.420458,'lng'=>-53.519399),
				array('lat'=>47.420535,'lng'=>-53.51891),
				array('lat'=>47.419696,'lng'=>-53.519039)
			)
		),
		'32' => array(
			'title' => 'Lot 32',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.419696,'lng'=>-53.519039),
				array('lat'=>47.420535,'lng'=>-53.51891),
				array('lat'=>47.420589,'lng'=>-53.518524),
				array('lat'=>47.419714,'lng'=>-53.518562)
			)
		),
		'33' => array(
			'title' => 'Lot 33',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.419714,'lng'=>-53.518562),
				array('lat'=>47.420589,'lng'=>-53.518524),
				array('lat'=>47.420814,'lng'=>-53.518084),
				array('lat'=>47.419714,'lng'=>-53.518127)
			)
		),
		'A1' => array(
			'title' => 'Lot A1',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.422215,'lng'=>-53.518037),
				array('lat'=>47.422823,'lng'=>-53.517825),
				array('lat'=>47.422799,'lng'=>-53.517242),
				array('lat'=>47.423006,'lng'=>-53.516652),
				array('lat'=>47.422854,'lng'=>-53.516421),
				array('lat'=>47.422597,'lng'=>-53.516397),
				array('lat'=>47.422108,'lng'=>-53.517584),
				array('lat'=>47.422114,'lng'=>-53.517679)
			)
		),
		'A2' => array(
			'title' => 'Lot A2',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.422053,'lng'=>-53.517711),
				array('lat'=>47.422597,'lng'=>-53.516397),
				array('lat'=>47.422367,'lng'=>-53.516378),
				array('lat'=>47.422222,'lng'=>-53.516454),
				array('lat'=>47.421958,'lng'=>-53.516527)
			)
		),
		'A3' => array(
			'title' => 'Lot A3',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.422053,'lng'=>-53.517711),
				array('lat'=>47.421958,'lng'=>-53.516527),
				array('lat'=>47.421565,'lng'=>-53.516631),
				array('lat'=>47.421358,'lng'=>-53.517135),
				array('lat'=>47.421148,'lng'=>-53.517441),
				array('lat'=>47.420996,'lng'=>-53.517526),
				array('lat'=>47.420829,'lng'=>-53.517698),
				array('lat'=>47.420614,'lng'=>-53.518092),
				array('lat'=>47.420814,'lng'=>-53.518084),
				array('lat'=>47.420887,'lng'=>-53.518004),
				array('lat'=>47.421467,'lng'=>-53.517585),
				array('lat'=>47.421779,'lng'=>-53.517591),
				array('lat'=>47.421936,'lng'=>-53.517773)
			)
		),
		'B1' => array(
			'title' => 'Lot B1',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.419718,'lng'=>-53.518041),
				array('lat'=>47.420498,'lng'=>-53.51802),
				array('lat'=>47.420679,'lng'=>-53.517584),
				array('lat'=>47.420539,'lng'=>-53.517128),
				array('lat'=>47.420342,'lng'=>-53.516829),
				array('lat'=>47.420131,'lng'=>-53.517204),
				array('lat'=>47.419725,'lng'=>-53.517693)
			)
		),
		'B2' => array(
			'title' => 'Lot B2',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.419725,'lng'=>-53.517693),
				array('lat'=>47.420131,'lng'=>-53.517204),
				array('lat'=>47.420342,'lng'=>-53.516829),
				array('lat'=>47.420193,'lng'=>-53.516711),
				array('lat'=>47.419896,'lng'=>-53.516652),
				array('lat'=>47.419674,'lng'=>-53.51684),
				array('lat'=>47.419453,'lng'=>-53.517264)
			)
		),
		'B3' => array(
			'title' => 'Lot B3',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.419725,'lng'=>-53.517693),
				array('lat'=>47.419453,'lng'=>-53.517264),
				array('lat'=>47.419163,'lng'=>-53.517457),
				array('lat'=>47.419032,'lng'=>-53.517382),
				array('lat'=>47.418916,'lng'=>-53.516856),
				array('lat'=>47.418215,'lng'=>-53.516791),
				array('lat'=>47.418161,'lng'=>-53.517929),
				array('lat'=>47.419678,'lng'=>-53.517875)
			)
		),
		'34' => array(
			'title' => 'Lot 34',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.418215,'lng'=>-53.516791),
				array('lat'=>47.418916,'lng'=>-53.516856),
				array('lat'=>47.419257,'lng'=>-53.516121),
				array('lat'=>47.420005,'lng'=>-53.515756),
				array('lat'=>47.420184,'lng'=>-53.515699),
				array('lat'=>47.419542,'lng'=>-53.515032),
				array('lat'=>47.419471,'lng'=>-53.515225),
				array('lat'=>47.419344,'lng'=>-53.515365),
				array('lat'=>47.419043,'lng'=>-53.515525),
				array('lat'=>47.418604,'lng'=>-53.515686),
				array('lat'=>47.418411,'lng'=>-53.515761),
				array('lat'=>47.418299,'lng'=>-53.515901),
				array('lat'=>47.418222,'lng'=>-53.516094),
				array('lat'=>47.418201,'lng'=>-53.516496)
			)
		),
		'35' => array(
			'title' => 'Lot 35',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.419542,'lng'=>-53.515032),
				array('lat'=>47.420184,'lng'=>-53.515699),
				array('lat'=>47.420792,'lng'=>-53.515511),
				array('lat'=>47.419737,'lng'=>-53.514388)
			)
		),
		'36' => array(
			'title' => 'Lot 36',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.420792,'lng'=>-53.515511),
				array('lat'=>47.421072,'lng'=>-53.514998),
				array('lat'=>47.419895,'lng'=>-53.513767),
				array('lat'=>47.419737,'lng'=>-53.514388)
			)
		),
		'37' => array(
			'title' => 'Lot 37',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.421072,'lng'=>-53.514998),
				array('lat'=>47.421069,'lng'=>-53.514376),
				array('lat'=>47.420056,'lng'=>-53.513271),
				array('lat'=>47.419895,'lng'=>-53.513767)
			)
		),
		'38' => array(
			'title' => 'Lot 38',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.421069,'lng'=>-53.514376),
				array('lat'=>47.421064,'lng'=>-53.513938),
				array('lat'=>47.420912,'lng'=>-53.513959),
				array('lat'=>47.420941,'lng'=>-53.513685),
				array('lat'=>47.420331,'lng'=>-53.512609),
				array('lat'=>47.42019,'lng'=>-53.512924),
				array('lat'=>47.420056,'lng'=>-53.513271)
			)
		),
		'39' => array(
			'title' => 'Lot 39',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.420941,'lng'=>-53.513685),
				array('lat'=>47.420974,'lng'=>-53.513321),
				array('lat'=>47.421118,'lng'=>-53.513041),
				array('lat'=>47.420607,'lng'=>-53.512047),
				array('lat'=>47.420331,'lng'=>-53.512609)
			)
		),
		'40' => array(
			'title' => 'Lot 40',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.421118,'lng'=>-53.513041),
				array('lat'=>47.421213,'lng'=>-53.512865),
				array('lat'=>47.421293,'lng'=>-53.513267),
				array('lat'=>47.421516,'lng'=>-53.51275),
				array('lat'=>47.420863,'lng'=>-53.511543),
				array('lat'=>47.420607,'lng'=>-53.512047)
			)
		),
		'41' => array(
			'title' => 'Lot 41',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.421516,'lng'=>-53.51275),
				array('lat'=>47.421642,'lng'=>-53.512468),
				array('lat'=>47.421645,'lng'=>-53.512178),
				array('lat'=>47.421693,'lng'=>-53.511965),
				array('lat'=>47.421119,'lng'=>-53.511013),
				array('lat'=>47.420863,'lng'=>-53.511543)
			)
		),
		'42' => array(
			'title' => 'Lot 42',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.421693,'lng'=>-53.511965),
				array('lat'=>47.421736,'lng'=>-53.511765),
				array('lat'=>47.422011,'lng'=>-53.511522),
				array('lat'=>47.42137,'lng'=>-53.510527),
				array('lat'=>47.421119,'lng'=>-53.511013)
			)
		),
		'43' => array(
			'title' => 'Lot 43',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.422011,'lng'=>-53.511522),
				array('lat'=>47.422331,'lng'=>-53.511255),
				array('lat'=>47.422363,'lng'=>-53.51114),
				array('lat'=>47.421633,'lng'=>-53.510001),
				array('lat'=>47.42137,'lng'=>-53.510527)
			)
		),
		'44' => array(
			'title' => 'Lot 44',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.422363,'lng'=>-53.51114),
				array('lat'=>47.422476,'lng'=>-53.51074),
				array('lat'=>47.422494,'lng'=>-53.51053),
				array('lat'=>47.421876,'lng'=>-53.509501),
				array('lat'=>47.421633,'lng'=>-53.510001)
			)
		),
		'45' => array(
			'title' => 'Lot 45',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.422494,'lng'=>-53.51053),
				array('lat'=>47.422609,'lng'=>-53.509961),
				array('lat'=>47.422113,'lng'=>-53.509024),
				array('lat'=>47.421876,'lng'=>-53.509501)
			)
		),
		'46' => array(
			'title' => 'Lot 46',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.422609,'lng'=>-53.509961),
				array('lat'=>47.422753,'lng'=>-53.509557),
				array('lat'=>47.42232,'lng'=>-53.508603),
				array('lat'=>47.422113,'lng'=>-53.509024)
			)
		),
		'47' => array(
			'title' => 'Lot 47',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.422753,'lng'=>-53.509557),
				array('lat'=>47.422938,'lng'=>-53.509221),
				array('lat'=>47.422516,'lng'=>-53.508205),
				array('lat'=>47.42232,'lng'=>-53.508603)
			)
		),
		'48' => array(
			'title' => 'Lot 48',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.422938,'lng'=>-53.509221),
				array('lat'=>47.422988,'lng'=>-53.509136),
				array('lat'=>47.423139,'lng'=>-53.509022),
				array('lat'=>47.422809,'lng'=>-53.507632),
				array('lat'=>47.422516,'lng'=>-53.508205)
			)
		),
		'49' => array(
			'title' => 'Lot 49',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.423139,'lng'=>-53.509022),
				array('lat'=>47.423322,'lng'=>-53.508884),
				array('lat'=>47.423285,'lng'=>-53.50732),
				array('lat'=>47.423108,'lng'=>-53.507339),
				array('lat'=>47.422952,'lng'=>-53.507452),
				array('lat'=>47.422809,'lng'=>-53.507632)
			)
		),
		'50' => array(
			'title' => 'Lot 50',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.423656,'lng'=>-53.508879),
				array('lat'=>47.423983,'lng'=>-53.507339),
				array('lat'=>47.423285,'lng'=>-53.50732),
				array('lat'=>47.423322,'lng'=>-53.508884)
			)
		),
		'51' => array(
			'title' => 'Lot 51',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.424368,'lng'=>-53.507345),
				array('lat'=>47.423983,'lng'=>-53.507339),
				array('lat'=>47.42366,'lng'=>-53.508883),
				array('lat'=>47.424153,'lng'=>-53.509251)
			)
		),
		'52' => array(
			'title' => 'Lot 52',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.424368,'lng'=>-53.507345),
				array('lat'=>47.424153,'lng'=>-53.509251),
				array('lat'=>47.424456,'lng'=>-53.50939),
				array('lat'=>47.424753,'lng'=>-53.509394),
				array('lat'=>47.424766,'lng'=>-53.507366)
			)
		),
		'53' => array(
			'title' => 'Lot 53',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.424766,'lng'=>-53.507366),
				array('lat'=>47.424753,'lng'=>-53.509394),
				array('lat'=>47.424964,'lng'=>-53.509392),
				array('lat'=>47.425156,'lng'=>-53.509374),
				array('lat'=>47.42515,'lng'=>-53.507394)
			)
		),
		'54' => array(
			'title' => 'Lot 54',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.42515,'lng'=>-53.507394),
				array('lat'=>47.425156,'lng'=>-53.509374),
				array('lat'=>47.425264,'lng'=>-53.509367),
				array('lat'=>47.425468,'lng'=>-53.509229),
				array('lat'=>47.425548,'lng'=>-53.509224),
				array('lat'=>47.425531,'lng'=>-53.507408)
			)
		),
		'55' => array(
			'title' => 'Lot 55',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.425531,'lng'=>-53.507408),
				array('lat'=>47.425548,'lng'=>-53.509224),
				array('lat'=>47.425661,'lng'=>-53.509217),
				array('lat'=>47.425846,'lng'=>-53.509276),
				array('lat'=>47.425921,'lng'=>-53.507438)
			)
		),
		'56' => array(
			'title' => 'Lot 56',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.425921,'lng'=>-53.507438),
				array('lat'=>47.425846,'lng'=>-53.509276),
				array('lat'=>47.426034,'lng'=>-53.50938),
				array('lat'=>47.426532,'lng'=>-53.507513),
				array('lat'=>47.426382,'lng'=>-53.507463)
			)
		),
		'57' => array(
			'title' => 'Lot 57',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.426532,'lng'=>-53.507513),
				array('lat'=>47.426034,'lng'=>-53.50938),
				array('lat'=>47.426077,'lng'=>-53.509706),
				array('lat'=>47.426163,'lng'=>-53.50987),
				array('lat'=>47.426992,'lng'=>-53.507728),
				array('lat'=>47.426603,'lng'=>-53.507516)
			)
		),
		'58' => array(
			'title' => 'Lot 58',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.426163,'lng'=>-53.50987),
				array('lat'=>47.426442,'lng'=>-53.510016),
				array('lat'=>47.427604,'lng'=>-53.508061),
				array('lat'=>47.426992,'lng'=>-53.507728)
			)
		),
		'59A' => array(
			'title' => 'Lot 59A',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.426521,'lng'=>-53.510019),
				array('lat'=>47.42666,'lng'=>-53.510261),
				array('lat'=>47.427921,'lng'=>-53.509334),
				array('lat'=>47.427994,'lng'=>-53.508966),
				array('lat'=>47.427193,'lng'=>-53.508856)
			)
		),
		'59B' => array(
			'title' => 'Lot 59B',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.426442,'lng'=>-53.510016),
				array('lat'=>47.426521,'lng'=>-53.510019),
				array('lat'=>47.427193,'lng'=>-53.508856),
				array('lat'=>47.427994,'lng'=>-53.508966),
				array('lat'=>47.428117,'lng'=>-53.508346),
				array('lat'=>47.427604,'lng'=>-53.508061)
			)
		),
		'60' => array(
			'title' => 'Lot 60',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.42666,'lng'=>-53.510261),
				array('lat'=>47.426808,'lng'=>-53.51034),
				array('lat'=>47.426837,'lng'=>-53.510471),
				array('lat'=>47.427805,'lng'=>-53.510334),
				array('lat'=>47.427921,'lng'=>-53.509334)
			)
		),
		'61' => array(
			'title' => 'Lot 61',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.426837,'lng'=>-53.510471),
				array('lat'=>47.426819,'lng'=>-53.510639),
				array('lat'=>47.426751,'lng'=>-53.510759),
				array('lat'=>47.427675,'lng'=>-53.511052),
				array('lat'=>47.427805,'lng'=>-53.510334)
			)
		),
		'62' => array(
			'title' => 'Lot 62',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.426751,'lng'=>-53.510759),
				array('lat'=>47.426426,'lng'=>-53.511042),
				array('lat'=>47.427501,'lng'=>-53.511511),
				array('lat'=>47.427675,'lng'=>-53.511052)
			)
		),
		'63' => array(
			'title' => 'Lot 63',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.426426,'lng'=>-53.511042),
				array('lat'=>47.42629,'lng'=>-53.511189),
				array('lat'=>47.426045,'lng'=>-53.511551),
				array('lat'=>47.427336,'lng'=>-53.511962),
				array('lat'=>47.427501,'lng'=>-53.511511)
			)
		),
		'64' => array(
			'title' => 'Lot 64',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.426045,'lng'=>-53.511551),
				array('lat'=>47.425944,'lng'=>-53.511903),
				array('lat'=>47.42592,'lng'=>-53.512287),
				array('lat'=>47.427176,'lng'=>-53.51239),
				array('lat'=>47.427336,'lng'=>-53.511962)
			)
		),
		'65' => array(
			'title' => 'Lot 65',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.427176,'lng'=>-53.51239),
				array('lat'=>47.42592,'lng'=>-53.512287),
				array('lat'=>47.425977,'lng'=>-53.512814),
				array('lat'=>47.426138,'lng'=>-53.513165),
				array('lat'=>47.42718,'lng'=>-53.51292)
			)
		),
		'66' => array(
			'title' => 'Lot 66',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.426138,'lng'=>-53.513165),
				array('lat'=>47.426474,'lng'=>-53.513842),
				array('lat'=>47.427308,'lng'=>-53.513478),
				array('lat'=>47.42718,'lng'=>-53.51292)
			)
		),
		'67' => array(
			'title' => 'Lot 67',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.426474,'lng'=>-53.513842),
				array('lat'=>47.426546,'lng'=>-53.514299),
				array('lat'=>47.427509,'lng'=>-53.51414),
				array('lat'=>47.427308,'lng'=>-53.513478)
			)
		),
		'68' => array(
			'title' => 'Lot 68',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.427233,'lng'=>-53.512513),
				array('lat'=>47.427886,'lng'=>-53.513079),
				array('lat'=>47.428134,'lng'=>-53.51272),
				array('lat'=>47.42739,'lng'=>-53.512006),
				array('lat'=>47.427273,'lng'=>-53.512331)
			)
		),
		'69' => array(
			'title' => 'Lot 69',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.428134,'lng'=>-53.51272),
				array('lat'=>47.428269,'lng'=>-53.512505),
				array('lat'=>47.428327,'lng'=>-53.512264),
				array('lat'=>47.427561,'lng'=>-53.51154),
				array('lat'=>47.42739,'lng'=>-53.512006)
			)
		),
		'70' => array(
			'title' => 'Lot 70',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.428327,'lng'=>-53.512264),
				array('lat'=>47.428343,'lng'=>-53.511695),
				array('lat'=>47.427719,'lng'=>-53.511092),
				array('lat'=>47.427561,'lng'=>-53.51154)
			)
		),
		'71' => array(
			'title' => 'Lot 71',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.428343,'lng'=>-53.511695),
				array('lat'=>47.428512,'lng'=>-53.51115),
				array('lat'=>47.427875,'lng'=>-53.510389),
				array('lat'=>47.427719,'lng'=>-53.511092)
			)
		),
		'72' => array(
			'title' => 'Lot 72',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.428512,'lng'=>-53.51115),
				array('lat'=>47.428603,'lng'=>-53.510853),
				array('lat'=>47.428632,'lng'=>-53.51052),
				array('lat'=>47.427953,'lng'=>-53.509547),
				array('lat'=>47.427875,'lng'=>-53.510389)
			)
		),
		'73' => array(
			'title' => 'Lot 73',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.428632,'lng'=>-53.51052),
				array('lat'=>47.428717,'lng'=>-53.510255),
				array('lat'=>47.428153,'lng'=>-53.508444),
				array('lat'=>47.427953,'lng'=>-53.509547)
			)
		),
		'206' => array(
			'title' => 'Lot 206',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.424077,'lng'=>-53.507248),
				array('lat'=>47.42412,'lng'=>-53.507012),
				array('lat'=>47.424088,'lng'=>-53.505832),
				array('lat'=>47.424498,'lng'=>-53.505876),
				array('lat'=>47.424463,'lng'=>-53.507241)
			)
		),
		'205' => array(
			'title' => 'Lot 205',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.424463,'lng'=>-53.507241),
				array('lat'=>47.424498,'lng'=>-53.505876),
				array('lat'=>47.424894,'lng'=>-53.505911),
				array('lat'=>47.424852,'lng'=>-53.507266)
			)
		),
		'204' => array(
			'title' => 'Lot 204',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.424852,'lng'=>-53.507266),
				array('lat'=>47.424894,'lng'=>-53.505911),
				array('lat'=>47.42528,'lng'=>-53.50595),
				array('lat'=>47.425222,'lng'=>-53.507297)
			)
		),
		'203' => array(
			'title' => 'Lot 203',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.425222,'lng'=>-53.507297),
				array('lat'=>47.42528,'lng'=>-53.50595),
				array('lat'=>47.425661,'lng'=>-53.505985),
				array('lat'=>47.425607,'lng'=>-53.50732)
			)
		),
		'202' => array(
			'title' => 'Lot 202',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.425607,'lng'=>-53.50732),
				array('lat'=>47.425661,'lng'=>-53.505985),
				array('lat'=>47.426047,'lng'=>-53.50602),
				array('lat'=>47.425985,'lng'=>-53.507345)
			)
		),
		'201' => array(
			'title' => 'Lot 201',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.425985,'lng'=>-53.507345),
				array('lat'=>47.426047,'lng'=>-53.50602),
				array('lat'=>47.426417,'lng'=>-53.50606),
				array('lat'=>47.426359,'lng'=>-53.507378)
			)
		),
		'200B' => array(
			'title' => 'Lot 200B',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.426359,'lng'=>-53.507378),
				array('lat'=>47.426417,'lng'=>-53.50606),
				array('lat'=>47.426892,'lng'=>-53.506103),
				array('lat'=>47.426788,'lng'=>-53.507506),
				array('lat'=>47.426607,'lng'=>-53.507425),
				array('lat'=>47.426451,'lng'=>-53.507397)
			)
		),
		'200A' => array(
			'title' => 'Lot 200A',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.426788,'lng'=>-53.507506),
				array('lat'=>47.426892,'lng'=>-53.506103),
				array('lat'=>47.427323,'lng'=>-53.506153),
				array('lat'=>47.427122,'lng'=>-53.507702)
			)
		),
		'199' => array(
			'title' => 'Lot 199',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.427122,'lng'=>-53.507702),
				array('lat'=>47.427323,'lng'=>-53.506153),
				array('lat'=>47.42785,'lng'=>-53.506197),
				array('lat'=>47.427753,'lng'=>-53.507157),
				array('lat'=>47.42765,'lng'=>-53.507248),
				array('lat'=>47.427594,'lng'=>-53.507366),
				array('lat'=>47.427536,'lng'=>-53.507527),
				array('lat'=>47.42745,'lng'=>-53.507878)
			)
		),
		'74' => array(
			'title' => 'Lot 74',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.428277,'lng'=>-53.508451),
				array('lat'=>47.42884,'lng'=>-53.510218),
				array('lat'=>47.429151,'lng'=>-53.510303),
				array('lat'=>47.429192,'lng'=>-53.508902)
			)
		),
		'75' => array(
			'title' => 'Lot 75',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.429192,'lng'=>-53.508902),
				array('lat'=>47.429151,'lng'=>-53.510303),
				array('lat'=>47.42915,'lng'=>-53.51089),
				array('lat'=>47.429519,'lng'=>-53.510999),
				array('lat'=>47.42955,'lng'=>-53.508912)
			)
		),
		'76' => array(
			'title' => 'Lot 76',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.42955,'lng'=>-53.508912),
				array('lat'=>47.429519,'lng'=>-53.510999),
				array('lat'=>47.429887,'lng'=>-53.510971),
				array('lat'=>47.429893,'lng'=>-53.508925)
			)
		),
		'77' => array(
			'title' => 'Lot 77',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.429893,'lng'=>-53.508925),
				array('lat'=>47.429887,'lng'=>-53.510971),
				array('lat'=>47.430038,'lng'=>-53.510807),
				array('lat'=>47.430237,'lng'=>-53.510754),
				array('lat'=>47.430239,'lng'=>-53.50893)
			)
		),
		'78' => array(
			'title' => 'Lot 78',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.430239,'lng'=>-53.50893),
				array('lat'=>47.430237,'lng'=>-53.510754),
				array('lat'=>47.430575,'lng'=>-53.510813),
				array('lat'=>47.430582,'lng'=>-53.508933)
			)
		),
		'79' => array(
			'title' => 'Lot 79',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.430582,'lng'=>-53.508933),
				array('lat'=>47.430575,'lng'=>-53.510813),
				array('lat'=>47.430882,'lng'=>-53.510807),
				array('lat'=>47.430907,'lng'=>-53.508978)
			)
		),
		'80' => array(
			'title' => 'Lot 80',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.430907,'lng'=>-53.508978),
				array('lat'=>47.430882,'lng'=>-53.510807),
				array('lat'=>47.43105,'lng'=>-53.51092),
				array('lat'=>47.431466,'lng'=>-53.509123),
				array('lat'=>47.431357,'lng'=>-53.509051)
			)
		),
		'81' => array(
			'title' => 'Lot 81',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.431466,'lng'=>-53.509123),
				array('lat'=>47.43105,'lng'=>-53.51092),
				array('lat'=>47.430983,'lng'=>-53.5114),
				array('lat'=>47.431248,'lng'=>-53.511655),
				array('lat'=>47.431856,'lng'=>-53.509603)
			)
		),
		'82' => array(
			'title' => 'Lot 82',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.431856,'lng'=>-53.509603),
				array('lat'=>47.431248,'lng'=>-53.511655),
				array('lat'=>47.431486,'lng'=>-53.512151),
				array('lat'=>47.432108,'lng'=>-53.509917)
			)
		),
		'83' => array(
			'title' => 'Lot 83',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.432108,'lng'=>-53.509917),
				array('lat'=>47.431486,'lng'=>-53.512151),
				array('lat'=>47.431912,'lng'=>-53.51213),
				array('lat'=>47.432389,'lng'=>-53.510177)
			)
		),
		'84' => array(
			'title' => 'Lot 84',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.432389,'lng'=>-53.510177),
				array('lat'=>47.431912,'lng'=>-53.51213),
				array('lat'=>47.432348,'lng'=>-53.512216),
				array('lat'=>47.4327,'lng'=>-53.510204)
			)
		),
		'85' => array(
			'title' => 'Lot 85',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.432794,'lng'=>-53.512012),
				array('lat'=>47.432979,'lng'=>-53.510209),
				array('lat'=>47.4327,'lng'=>-53.510204),
				array('lat'=>47.432348,'lng'=>-53.512216)
			)
		),
		'86' => array(
			'title' => 'Lot 86',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.433235,'lng'=>-53.511854),
				array('lat'=>47.433266,'lng'=>-53.510217),
				array('lat'=>47.432979,'lng'=>-53.510209),
				array('lat'=>47.432794,'lng'=>-53.512012)
			)
		),
		'87' => array(
			'title' => 'Lot 87',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.433655,'lng'=>-53.512252),
				array('lat'=>47.43328,'lng'=>-53.511891),
				array('lat'=>47.433306,'lng'=>-53.510215),
				array('lat'=>47.433671,'lng'=>-53.510222)
			)
		),
		'88' => array(
			'title' => 'Lot 88',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.433986,'lng'=>-53.511837),
				array('lat'=>47.433835,'lng'=>-53.512166),
				array('lat'=>47.433655,'lng'=>-53.512252),
				array('lat'=>47.433671,'lng'=>-53.510222),
				array('lat'=>47.433993,'lng'=>-53.51022)
			)
		),
		'89' => array(
			'title' => 'Lot 89',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.434336,'lng'=>-53.51022),
				array('lat'=>47.434309,'lng'=>-53.511918),
				array('lat'=>47.433986,'lng'=>-53.511837),
				array('lat'=>47.433993,'lng'=>-53.51022)
			)
		),
		'90' => array(
			'title' => 'Lot 90',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.434681,'lng'=>-53.510217),
				array('lat'=>47.434674,'lng'=>-53.511596),
				array('lat'=>47.434309,'lng'=>-53.511918),
				array('lat'=>47.434336,'lng'=>-53.51022)
			)
		),
		'91' => array(
			'title' => 'Lot 91',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.435071,'lng'=>-53.510228),
				array('lat'=>47.435044,'lng'=>-53.51155),
				array('lat'=>47.434674,'lng'=>-53.511596),
				array('lat'=>47.434681,'lng'=>-53.510217)
			)
		),
		'92' => array(
			'title' => 'Lot 92',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.435439,'lng'=>-53.510303),
				array('lat'=>47.435321,'lng'=>-53.511963),
				array('lat'=>47.435044,'lng'=>-53.51155),
				array('lat'=>47.435071,'lng'=>-53.510228),
				array('lat'=>47.435283,'lng'=>-53.510242)
			)
		),
		'166' => array(
			'title' => 'Lot 166',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.427568,'lng'=>-53.507956),
				array('lat'=>47.427641,'lng'=>-53.507621),
				array('lat'=>47.427703,'lng'=>-53.507441),
				array('lat'=>47.427779,'lng'=>-53.507366),
				array('lat'=>47.427993,'lng'=>-53.507355),
				array('lat'=>47.428359,'lng'=>-53.507431),
				array('lat'=>47.428134,'lng'=>-53.508262)
			)
		),
		'165' => array(
			'title' => 'Lot 165',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.428134,'lng'=>-53.508262),
				array('lat'=>47.428359,'lng'=>-53.507431),
				array('lat'=>47.428703,'lng'=>-53.507469),
				array('lat'=>47.428648,'lng'=>-53.508546)
			)
		),
		'164' => array(
			'title' => 'Lot 164',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.428648,'lng'=>-53.508546),
				array('lat'=>47.428703,'lng'=>-53.507469),
				array('lat'=>47.429189,'lng'=>-53.507517),
				array('lat'=>47.42914,'lng'=>-53.508804)
			)
		),
		'163' => array(
			'title' => 'Lot 163',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.42914,'lng'=>-53.508804),
				array('lat'=>47.429189,'lng'=>-53.507517),
				array('lat'=>47.429523,'lng'=>-53.507549),
				array('lat'=>47.429477,'lng'=>-53.508825)
			)
		),
		'162' => array(
			'title' => 'Lot 162',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.429477,'lng'=>-53.508825),
				array('lat'=>47.429523,'lng'=>-53.507549),
				array('lat'=>47.429843,'lng'=>-53.507584),
				array('lat'=>47.429797,'lng'=>-53.508843)
			)
		),
		'161' => array(
			'title' => 'Lot 161',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.429797,'lng'=>-53.508843),
				array('lat'=>47.429841,'lng'=>-53.507583),
				array('lat'=>47.430159,'lng'=>-53.507611),
				array('lat'=>47.430119,'lng'=>-53.508856)
			)
		),
		'160' => array(
			'title' => 'Lot 160',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.430119,'lng'=>-53.508856),
				array('lat'=>47.430159,'lng'=>-53.507611),
				array('lat'=>47.430476,'lng'=>-53.507647),
				array('lat'=>47.43044,'lng'=>-53.508865)
			)
		),
		'159' => array(
			'title' => 'Lot 159',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.43044,'lng'=>-53.508865),
				array('lat'=>47.430476,'lng'=>-53.507647),
				array('lat'=>47.430794,'lng'=>-53.507678),
				array('lat'=>47.430757,'lng'=>-53.508883)
			)
		),
		'158' => array(
			'title' => 'Lot 158',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.430757,'lng'=>-53.508883),
				array('lat'=>47.430794,'lng'=>-53.507678),
				array('lat'=>47.431115,'lng'=>-53.507712),
				array('lat'=>47.431075,'lng'=>-53.508903)
			)
		),
		'157' => array(
			'title' => 'Lot 157',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.431075,'lng'=>-53.508903),
				array('lat'=>47.431115,'lng'=>-53.507712),
				array('lat'=>47.43144,'lng'=>-53.50774),
				array('lat'=>47.43138,'lng'=>-53.50896)
			)
		),
		'156' => array(
			'title' => 'Lot 156',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.431635,'lng'=>-53.509158),
				array('lat'=>47.431706,'lng'=>-53.507747),
				array('lat'=>47.431439,'lng'=>-53.507739),
				array('lat'=>47.43138,'lng'=>-53.50896)
			)
		),
		'155' => array(
			'title' => 'Lot 155',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.431948,'lng'=>-53.509574),
				array('lat'=>47.432252,'lng'=>-53.508625),
				array('lat'=>47.431675,'lng'=>-53.508344),
				array('lat'=>47.431635,'lng'=>-53.509158)
			)
		),
		'154' => array(
			'title' => 'Lot 154',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.432239,'lng'=>-53.509941),
				array('lat'=>47.432571,'lng'=>-53.50882),
				array('lat'=>47.432252,'lng'=>-53.508625),
				array('lat'=>47.431948,'lng'=>-53.509574)
			)
		),
		'153' => array(
			'title' => 'Lot 153',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.432727,'lng'=>-53.510118),
				array('lat'=>47.432783,'lng'=>-53.508957),
				array('lat'=>47.432571,'lng'=>-53.50882),
				array('lat'=>47.432239,'lng'=>-53.509941),
				array('lat'=>47.432417,'lng'=>-53.510086)
			)
		),
		'152' => array(
			'title' => 'Lot 152',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.433035,'lng'=>-53.510126),
				array('lat'=>47.433077,'lng'=>-53.508973),
				array('lat'=>47.432783,'lng'=>-53.508957),
				array('lat'=>47.432727,'lng'=>-53.510118)
			)
		),
		'151' => array(
			'title' => 'Lot 151',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.433343,'lng'=>-53.510126),
				array('lat'=>47.433379,'lng'=>-53.508991),
				array('lat'=>47.433077,'lng'=>-53.508973),
				array('lat'=>47.433035,'lng'=>-53.510126)
			)
		),
		'150' => array(
			'title' => 'Lot 150',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.433653,'lng'=>-53.510132),
				array('lat'=>47.433686,'lng'=>-53.509006),
				array('lat'=>47.433379,'lng'=>-53.508991),
				array('lat'=>47.433343,'lng'=>-53.510126)
			)
		),
		'149' => array(
			'title' => 'Lot 149',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.433965,'lng'=>-53.510133),
				array('lat'=>47.434,'lng'=>-53.509016),
				array('lat'=>47.433686,'lng'=>-53.509006),
				array('lat'=>47.433653,'lng'=>-53.510132)
			)
		),
		'148' => array(
			'title' => 'Lot 148',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.434366,'lng'=>-53.510135),
				array('lat'=>47.434397,'lng'=>-53.509037),
				array('lat'=>47.43409,'lng'=>-53.50902),
				array('lat'=>47.434056,'lng'=>-53.510123)
			)
		),
		'147' => array(
			'title' => 'Lot 147',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.43468,'lng'=>-53.510135),
				array('lat'=>47.434707,'lng'=>-53.509051),
				array('lat'=>47.434397,'lng'=>-53.509037),
				array('lat'=>47.434366,'lng'=>-53.510135)
			)
		),
		'146' => array(
			'title' => 'Lot 146',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.434985,'lng'=>-53.51014),
				array('lat'=>47.435014,'lng'=>-53.509064),
				array('lat'=>47.434707,'lng'=>-53.509051),
				array('lat'=>47.43468,'lng'=>-53.510135)
			)
		),
		'145' => array(
			'title' => 'Lot 145',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.435301,'lng'=>-53.510144),
				array('lat'=>47.435321,'lng'=>-53.509085),
				array('lat'=>47.435014,'lng'=>-53.509064),
				array('lat'=>47.434985,'lng'=>-53.51014)
			)
		),
		'G2' => array(
			'title' => 'Lot G2',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.435439324,'lng'=>-53.510300517082),
				array('lat'=>47.435321394285,'lng'=>-53.511963486671),
				array('lat'=>47.435722354237,'lng'=>-53.512156605721),
				array('lat'=>47.435728594973,'lng'=>-53.512436971433),
				array('lat'=>47.435585070156,'lng'=>-53.51282039806),
				array('lat'=>47.435531165513,'lng'=>-53.513131318523),
				array('lat'=>47.435598031779,'lng'=>-53.513322017083),
				array('lat'=>47.435829466145,'lng'=>-53.513582320049),
				array('lat'=>47.435967937145,'lng'=>-53.51366804398),
				array('lat'=>47.436138991642,'lng'=>-53.513614621659),
				array('lat'=>47.436131992092,'lng'=>-53.513362115034),
				array('lat'=>47.436128046611,'lng'=>-53.513075202826),
				array('lat'=>47.436208165485,'lng'=>-53.512779450292),
				array('lat'=>47.436265475873,'lng'=>-53.512544301805),
				array('lat'=>47.436257567511,'lng'=>-53.510732352734),
				array('lat'=>47.4359364402,'lng'=>-53.510705530643),
				array('lat'=>47.435753197184,'lng'=>-53.510595560074)
			)
		),
		'94' => array(
			'title' => 'Lot 94',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.436257567511,'lng'=>-53.510732352734),
				array('lat'=>47.436265475873,'lng'=>-53.512544301805),
				array('lat'=>47.436678477177,'lng'=>-53.512550890446),
				array('lat'=>47.436573250076,'lng'=>-53.510662615299)
			)
		),
		'95' => array(
			'title' => 'Lot 95',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.436573250076,'lng'=>-53.510662615299),
				array('lat'=>47.436678477177,'lng'=>-53.512550890446),
				array('lat'=>47.43699959996,'lng'=>-53.511762320995),
				array('lat'=>47.436881673742,'lng'=>-53.510480225086)
			)
		),
		'96' => array(
			'title' => 'Lot 96',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.436881673742,'lng'=>-53.510480225086),
				array('lat'=>47.43699959996,'lng'=>-53.511762320995),
				array('lat'=>47.437295321467,'lng'=>-53.511456549168),
				array('lat'=>47.437173767429,'lng'=>-53.510212004185)
			)
		),
		'97' => array(
			'title' => 'Lot 97',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.437173767429,'lng'=>-53.510212004185),
				array('lat'=>47.437295321467,'lng'=>-53.511456549168),
				array('lat'=>47.437658167671,'lng'=>-53.511215150356),
				array('lat'=>47.437580155948,'lng'=>-53.509973287582),
				array('lat'=>47.437367890908,'lng'=>-53.510048389435)
			)
		),
		'98' => array(
			'title' => 'Lot 98',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.437658167671,'lng'=>-53.511215150356),
				array('lat'=>47.437881316844,'lng'=>-53.511207103729),
				array('lat'=>47.438019197161,'lng'=>-53.509930372238),
				array('lat'=>47.437580155948,'lng'=>-53.509973287582)
			)
		),
		'99' => array(
			'title' => 'Lot 99',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.437881316844,'lng'=>-53.511207103729),
				array('lat'=>47.438342125962,'lng'=>-53.511300981045),
				array('lat'=>47.438465491498,'lng'=>-53.510018885136),
				array('lat'=>47.438226960392,'lng'=>-53.509947381768),
				array('lat'=>47.438019197161,'lng'=>-53.509930372238)
			)
		),
		'100' => array(
			'title' => 'Lot 100',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.438465491498,'lng'=>-53.510018885136),
				array('lat'=>47.438342125962,'lng'=>-53.511300981045),
				array('lat'=>47.438514474792,'lng'=>-53.511410951614),
				array('lat'=>47.438616069628,'lng'=>-53.511555790901),
				array('lat'=>47.438905067667,'lng'=>-53.510281254343),
				array('lat'=>47.438703150759,'lng'=>-53.510118126869)
			)
		),
		'101' => array(
			'title' => 'Lot 101',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.438905067667,'lng'=>-53.510281254343),
				array('lat'=>47.438616069628,'lng'=>-53.511555790901),
				array('lat'=>47.438744256371,'lng'=>-53.511705722647),
				array('lat'=>47.438951694029,'lng'=>-53.511789143085),
				array('lat'=>47.439231077032,'lng'=>-53.51057946682)
			)
		),
		'102' => array(
			'title' => 'Lot 102',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.439231077032,'lng'=>-53.51057946682),
				array('lat'=>47.438951694029,'lng'=>-53.511789143085),
				array('lat'=>47.439176651888,'lng'=>-53.511866927147),
				array('lat'=>47.439325413815,'lng'=>-53.512014448643),
				array('lat'=>47.439563069192,'lng'=>-53.510861098766)
			)
		),
		'103' => array(
			'title' => 'Lot 103',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.439563069192,'lng'=>-53.510861098766),
				array('lat'=>47.439325413815,'lng'=>-53.512014448643),
				array('lat'=>47.439555812555,'lng'=>-53.512465059757),
				array('lat'=>47.439786210286,'lng'=>-53.510989844799)
			)
		),
		'104' => array(
			'title' => 'Lot 104',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.439786210286,'lng'=>-53.510989844799),
				array('lat'=>47.439555812555,'lng'=>-53.512465059757),
				array('lat'=>47.439606608992,'lng'=>-53.512644767761),
				array('lat'=>47.439807980098,'lng'=>-53.51278424263),
				array('lat'=>47.44000753629,'lng'=>-53.512872755527),
				array('lat'=>47.440120013083,'lng'=>-53.512829840183),
				array('lat'=>47.440147225174,'lng'=>-53.511022031307)
			)
		),
		'105' => array(
			'title' => 'Lot 105',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.440147225174,'lng'=>-53.511022031307),
				array('lat'=>47.440120013083,'lng'=>-53.512829840183),
				array('lat'=>47.44042297357,'lng'=>-53.512296080589),
				array('lat'=>47.440569917849,'lng'=>-53.511022031307)
			)
		),
		'106' => array(
			'title' => 'Lot 106',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.440569917849,'lng'=>-53.511022031307),
				array('lat'=>47.44042297357,'lng'=>-53.512296080589),
				array('lat'=>47.440546334227,'lng'=>-53.512400686741),
				array('lat'=>47.440675136957,'lng'=>-53.512695729733),
				array('lat'=>47.440805753488,'lng'=>-53.512800335884),
				array('lat'=>47.440813009953,'lng'=>-53.511019349098)
			)
		),
		'107' => array(
			'title' => 'Lot 107',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.440805753488,'lng'=>-53.512800335884),
				array('lat'=>47.441009143899,'lng'=>-53.512941272356),
				array('lat'=>47.441261902819,'lng'=>-53.512715862446),
				array('lat'=>47.441134107502,'lng'=>-53.511019349098),
				array('lat'=>47.440813009953,'lng'=>-53.511019349098)
			)
		),
		'108' => array(
			'title' => 'Lot 108',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.441134107502,'lng'=>-53.511019349098),
				array('lat'=>47.441261902819,'lng'=>-53.512715862446),
				array('lat'=>47.441511439971,'lng'=>-53.512722551823),
				array('lat'=>47.441896026048,'lng'=>-53.512711822987),
				array('lat'=>47.441455203092,'lng'=>-53.51101398468)
			)
		),
		'109' => array(
			'title' => 'Lot 109',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.441455203092,'lng'=>-53.51101398468),
				array('lat'=>47.441896026048,'lng'=>-53.512711822987),
				array('lat'=>47.442257026458,'lng'=>-53.512776196003),
				array('lat'=>47.442402151554,'lng'=>-53.512843251228),
				array('lat'=>47.442275167117,'lng'=>-53.512711822987),
				array('lat'=>47.442113715034,'lng'=>-53.512432873249),
				array('lat'=>47.442061106945,'lng'=>-53.512183427811),
				array('lat'=>47.442081061744,'lng'=>-53.511942028999),
				array('lat'=>47.442135483883,'lng'=>-53.51183205843),
				array('lat'=>47.441745457305,'lng'=>-53.510979115963)
			)
		),
		'132' => array(
			'title' => 'Lot 132',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.439909572436,'lng'=>-53.509648740292),
				array('lat'=>47.439598473388,'lng'=>-53.509647338857),
				array('lat'=>47.439576675383,'lng'=>-53.510783314705),
				array('lat'=>47.439785168986,'lng'=>-53.510913869373),
				array('lat'=>47.43991320073,'lng'=>-53.51093351841)
			)
		),
		'133' => array(
			'title' => 'Lot 133',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.439576675383,'lng'=>-53.510783314705),
				array('lat'=>47.439598473388,'lng'=>-53.509647338857),
				array('lat'=>47.439315727834,'lng'=>-53.509429291391),
				array('lat'=>47.439290417421,'lng'=>-53.510523372649)
			)
		),
		'134' => array(
			'title' => 'Lot 134',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.439290417421,'lng'=>-53.510523372649),
				array('lat'=>47.439315727834,'lng'=>-53.509429291391),
				array('lat'=>47.439038941067,'lng'=>-53.509239267884),
				array('lat'=>47.438999867191,'lng'=>-53.510272067838)
			)
		),
		'135' => array(
			'title' => 'Lot 135',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.438999867191,'lng'=>-53.510272067838),
				array('lat'=>47.439038941067,'lng'=>-53.509239267884),
				array('lat'=>47.438734330265,'lng'=>-53.509010868742),
				array('lat'=>47.438706536638,'lng'=>-53.510026393683)
			)
		),
		'136' => array(
			'title' => 'Lot 136',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.438706536638,'lng'=>-53.510026393683),
				array('lat'=>47.438734330265,'lng'=>-53.509010868742),
				array('lat'=>47.438395982264,'lng'=>-53.508986861241),
				array('lat'=>47.438384761543,'lng'=>-53.509904830419)
			)
		),
		'137' => array(
			'title' => 'Lot 137',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.438384761543,'lng'=>-53.509904830419),
				array('lat'=>47.438395982264,'lng'=>-53.508986861241),
				array('lat'=>47.43798383326,'lng'=>-53.508955013096),
				array('lat'=>47.43798190833,'lng'=>-53.509840735982)
			)
		),
		'138' => array(
			'title' => 'Lot 138',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.43798190833,'lng'=>-53.509840735982),
				array('lat'=>47.43798383326,'lng'=>-53.508955013096),
				array('lat'=>47.437579190775,'lng'=>-53.508925112606),
				array('lat'=>47.43758457831,'lng'=>-53.509889931773)
			)
		),
		'139' => array(
			'title' => 'Lot 139',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.43758457831,'lng'=>-53.509889931773),
				array('lat'=>47.437579190775,'lng'=>-53.508925112606),
				array('lat'=>47.437139435969,'lng'=>-53.509195075847),
				array('lat'=>47.437146457901,'lng'=>-53.510133010045)
			)
		),
		'140' => array(
			'title' => 'Lot 140',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.436832688927,'lng'=>-53.509431481361),
				array('lat'=>47.436821803407,'lng'=>-53.510402441025),
				array('lat'=>47.437146457901,'lng'=>-53.510133010045),
				array('lat'=>47.437139435969,'lng'=>-53.509195075847)
			)
		),
		'141' => array(
			'title' => 'Lot 141',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.43650441087,'lng'=>-53.510571541216),
				array('lat'=>47.436507816635,'lng'=>-53.509465685486),
				array('lat'=>47.436832688927,'lng'=>-53.509431481361),
				array('lat'=>47.436821803407,'lng'=>-53.510402441025)
			)
		),
		'142' => array(
			'title' => 'Lot 142',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.436134207953,'lng'=>-53.510635257275),
				array('lat'=>47.436145221106,'lng'=>-53.509449540441),
				array('lat'=>47.436507816635,'lng'=>-53.509465685486),
				array('lat'=>47.43650441087,'lng'=>-53.510571541216)
			)
		),
		'143' => array(
			'title' => 'Lot 143',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.435749582758,'lng'=>-53.510458083818),
				array('lat'=>47.435771247106,'lng'=>-53.509434160046),
				array('lat'=>47.436145221106,'lng'=>-53.509449540441),
				array('lat'=>47.436134207953,'lng'=>-53.510635257275)
			)
		),
		'144' => array(
			'title' => 'Lot 144',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.435301,'lng'=>-53.510144),
				array('lat'=>47.435321,'lng'=>-53.509085),
				array('lat'=>47.435771247106,'lng'=>-53.509434160046),
				array('lat'=>47.435749582758,'lng'=>-53.510458083818)
			)
		),
		'118' => array(
			'title' => 'Lot 118',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.444578979939,'lng'=>-53.508219122887),
				array('lat'=>47.444894612578,'lng'=>-53.508449792862),
				array('lat'=>47.445090521539,'lng'=>-53.509667515755),
				array('lat'=>47.444589863855,'lng'=>-53.509581685066),
				array('lat'=>47.444390325044,'lng'=>-53.5084015131)
			)
		),
		'117' => array(
			'title' => 'Lot 117',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.444390325044,'lng'=>-53.5084015131),
				array('lat'=>47.444589863855,'lng'=>-53.509581685066),
				array('lat'=>47.444194413476,'lng'=>-53.509812355042),
				array('lat'=>47.444150877473,'lng'=>-53.50848197937)
			)
		),
		'116' => array(
			'title' => 'Lot 116',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.444150877473,'lng'=>-53.50848197937),
				array('lat'=>47.444194413476,'lng'=>-53.509812355042),
				array('lat'=>47.4438243563,'lng'=>-53.50984454155),
				array('lat'=>47.443907800792,'lng'=>-53.508540987968)
			)
		),
		'115' => array(
			'title' => 'Lot 115',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.443907800792,'lng'=>-53.508540987968),
				array('lat'=>47.4438243563,'lng'=>-53.50984454155),
				array('lat'=>47.443450668471,'lng'=>-53.50987136364),
				array('lat'=>47.443599418313,'lng'=>-53.508669734001)
			)
		),
		'114' => array(
			'title' => 'Lot 114',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.443599418313,'lng'=>-53.508669734001),
				array('lat'=>47.443450668471,'lng'=>-53.50987136364),
				array('lat'=>47.443066093758,'lng'=>-53.50989818573),
				array('lat'=>47.443185820149,'lng'=>-53.509045243263)
			)
		),
		'113' => array(
			'title' => 'Lot 113',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.443066093758,'lng'=>-53.50989818573),
				array('lat'=>47.442752471997,'lng'=>-53.510185454428),
				array('lat'=>47.44266169692,'lng'=>-53.509198135147),
				array('lat'=>47.4427960239,'lng'=>-53.508975665365),
				array('lat'=>47.443185820149,'lng'=>-53.509045243263)
			)
		),
		'112B' => array(
			'title' => 'Lot 112B',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.4427960239,'lng'=>-53.508975665365),
				array('lat'=>47.44266169692,'lng'=>-53.509198135147),
				array('lat'=>47.442752471997,'lng'=>-53.510185454428),
				array('lat'=>47.442569044918,'lng'=>-53.51134121418),
				array('lat'=>47.442315076545,'lng'=>-53.511035442352),
				array('lat'=>47.442438432765,'lng'=>-53.510745763779),
				array('lat'=>47.44235209565,'lng'=>-53.510211384371),
				array('lat'=>47.442340473437,'lng'=>-53.509345650673),
				array('lat'=>47.44264435276,'lng'=>-53.508940256336)
			)
		),
		'112' => array(
			'title' => 'Lot 112',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.44264435276,'lng'=>-53.508940256336),
				array('lat'=>47.442340473437,'lng'=>-53.509345650673),
				array('lat'=>47.44235209565,'lng'=>-53.510211384371),
				array('lat'=>47.441951374528,'lng'=>-53.50982588346),
				array('lat'=>47.442015247975,'lng'=>-53.508871786653)
			)
		),
		'111' => array(
			'title' => 'Lot 111',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.441951374528,'lng'=>-53.50982588346),
				array('lat'=>47.44235209565,'lng'=>-53.510211384371),
				array('lat'=>47.442438432765,'lng'=>-53.510745763779),
				array('lat'=>47.442315076545,'lng'=>-53.511035442352),
				array('lat'=>47.44191016783,'lng'=>-53.510534066097)
			)
		),
		'110' => array(
			'title' => 'Lot 110',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.44191016783,'lng'=>-53.510534066097),
				array('lat'=>47.441745457305,'lng'=>-53.510979115963),
				array('lat'=>47.442135483883,'lng'=>-53.51183205843),
				array('lat'=>47.442569044918,'lng'=>-53.51134121418),
				array('lat'=>47.442315076545,'lng'=>-53.511035442352)
			)
		),
		'301' => array(
			'title' => 'Lot 301',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.41078626878069,'lng'=>-53.52655202150345),
				array('lat'=>47.41043231312495,'lng'=>-53.527372777462006),
				array('lat'=>47.410014823909265,'lng'=>-53.52739691734314),
				array('lat'=>47.4100238321323,'lng'=>-53.5268946247993),
				array('lat'=>47.410597492727,'lng'=>-53.52622747421265)
			)
		),
		'302' => array(
			'title' => 'Lot 302',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.410597492727,'lng'=>-53.52622747421265),
				array('lat'=>47.4100238321323,'lng'=>-53.5268946247993),
				array('lat'=>47.409965651992174,'lng'=>-53.526307292229035),
				array('lat'=>47.40982876786659,'lng'=>-53.525839895009995),
				array('lat'=>47.41050484175538,'lng'=>-53.52551562737597)
			)
		),
		'303' => array(
			'title' => 'Lot 303',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'available',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.41050484175538,'lng'=>-53.52551562737597),
				array('lat'=>47.40982876786659,'lng'=>-53.525839895009995),
				array('lat'=>47.40967582046768,'lng'=>-53.52531425244376),
				array('lat'=>47.4104909747945,'lng'=>-53.5245979841452)
			)
		),
		'304' => array(
			'title' => 'Lot 304',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.4104909747945,'lng'=>-53.5245979841452),
				array('lat'=>47.40967582046768,'lng'=>-53.52531425244376),
				array('lat'=>47.409507079797734,'lng'=>-53.5248567330525),
				array('lat'=>47.41006589833144,'lng'=>-53.524251231050584)
			)
		),
		'305' => array(
			'title' => 'Lot 305',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.409599146576184,'lng'=>-53.523507714271545),
				array('lat'=>47.409217954871814,'lng'=>-53.52426141500473),
				array('lat'=>47.409507079797734,'lng'=>-53.5248567330525),
				array('lat'=>47.41006589833144,'lng'=>-53.524251231050584)
			)
		),
		'306' => array(
			'title' => 'Lot 306',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.408918445168624,'lng'=>-53.52313756942749),
				array('lat'=>47.408744184193935,'lng'=>-53.52370083332062),
				array('lat'=>47.409217954871814,'lng'=>-53.52426141500473),
				array('lat'=>47.409599146576184,'lng'=>-53.523507714271545)
			)
		),
		'307' => array(
			'title' => 'Lot 307',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.40858081400655,'lng'=>-53.52244555950165),
				array('lat'=>47.40817420156227,'lng'=>-53.52319657802582),
				array('lat'=>47.408744184193935,'lng'=>-53.52370083332062),
				array('lat'=>47.408918445168624,'lng'=>-53.52313756942749)
			)
		),
		'308' => array(
			'title' => 'Lot 308',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.40854813990826,'lng'=>-53.521801829338074),
				array('lat'=>47.40782468743725,'lng'=>-53.52270442599689),
				array('lat'=>47.40817420156227,'lng'=>-53.52319657802582),
				array('lat'=>47.40858081400655,'lng'=>-53.52244555950165)
			)
		),
		'309' => array(
			'title' => 'Lot 309',
			'frontage'=>'100ft',
			'acreage'=> '0.75',
			'depth' => '240ft',
			'size'=> '4500 sq. ft.',
			'price' => 99900,
			'status' => 'sold',
			'image' =>"http://dev.me/gooseweb/assets/dist/images/temp/lakefront-header.jpg",
			'excerpt'=> "Lorem ipsum dolor sit amet, consectetur adipiscing elit.consectetur adipiscing elit.",
			'link'=> "https://www.youtube.com/watch?v=VKHV0LLvhXM",
			'path' => array(
				array('lat'=>47.408577183552175,'lng'=>-53.52116346359253),
				array('lat'=>47.407557015961075,'lng'=>-53.52193057537079),
				array('lat'=>47.40754249385984,'lng'=>-53.52230608463287),
				array('lat'=>47.40782468743725,'lng'=>-53.52270442599689),
				array('lat'=>47.40854813990826,'lng'=>-53.521801829338074)
			)
		)
	);
	
	$requested_lots = $_GET['lots'];
	$returned_lots = array();
	foreach($requested_lots as $lot_id) {
		$returned_lots[] = $lots[$lot_id];
	}
	echo json_encode($returned_lots);