			<footer class="dark-bg">
				<div class="sw">
				
					<div class="footer-blocks">
						<div class="footer-block">
						
							<a href="#" class="footer-logo">
								<img src="../assets/dist/images/goose-pond-country-properties.svg" alt="Goose Pond Country Properties Logo">
							</a>
						
							<div class="footer-contact">
								
								<address>
									123 Your Street <br />
									Whitebourne, NL A1B 2C3
								</address>
								
								<div class="rows">
									<div class="row">
										<span class="l">P</span>
										<span class="r">1 (555) 555-5555</span>
									</div>
									<div class="row">
										<span class="l">TF</span>
										<span class="r">1 (800) 555-5555</span>
									</div>
								</div>
								
							</div><!-- .footer-contact -->
						
						</div><!-- .footer-block -->
						
						<div class="footer-block">
						
							<div class="footer-nav">
								<ul>
									<li><a href="#">Goose Pond Properties</a></li>
									<li><a href="#">Services</a></li>
									<li><a href="#">Other Projects</a></li>
									<li><a href="#">The Latest</a></li>
								</ul>
							</div><!-- .footer-nav -->
							
						</div><!-- .footer-block -->
						
						<div class="footer-block">
						
							<div class="footer-callout">
								<p>Find your dream property today at Goose Pond.</p>
								<a href="#" class="button big">Now Selling</a>
							</div><!-- .center -->
						
						
						</div><!-- .footer-block -->
						
					</div><!-- .footer-blocks -->
				
				</div><!-- .sw -->
				
				<div class="copyright">
					<div class="sw">							
						<ul>
							<li>Copyright &copy; <?php echo date('Y'); ?> <a href="/">Goose Pond Properties</a></li>
							<li><a href="#">Sitemap</a></li>
							<li><a href="#">Legal</a></li>
						</ul>
						
						<a href="http://jac.co" rel="external" title="JAC. We Create." id="jac"><img src="../assets/dist/images/jac-logo.svg" alt="JAC Logo."></a>
					</div><!-- .sw -->
				</div><!-- .copyright -->
			</footer><!-- .footer -->
		
		</div><!-- .page-wrapper -->
		
		<div class="t-fa-abs fa-refresh gooseweb-map-overlay">
		
			<div class="gooseweb-map-header dark-bg">
			
				<div class="selector with-arrow">
					<select id="phase-selector">
						<option value="">Select A Phase</option>
						<option value="">Show All Phases</option>
					</select>
					<span class="value">&nbsp;</span>
				</div><!-- .selector -->
				
				<button class="t-fa-abs fa-close toggle-map-overlay">Close</button>
				
			</div><!-- .gooseweb-map-header -->
		
			<div class="gooseweb-map" data-center="44.9868465,-64.1363655" data-zoom="12">
			</div><!-- .gooseweb-map -->
			
			<div class="mobile-infowindow custom-info-window-content"></div>
		</div><!-- #gooseweb-map-overlay -->
			
		<script>
			var templateJS = {
				templateURL: 'http://dev.me/gooseweb/templates',
				CACHE_BUSTER: '<?php echo time(); ?>'	/* cache buster - set this to some unique string whenever an update is performed on js/css files, or when an admin is logged in */
			};
		</script>
		
		<script src="../assets/dist/js/main.js"></script>
	</body>
</html>