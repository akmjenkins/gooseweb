<div class="mobile-nav-bg">&nbsp;</div>

<button class="toggle-nav">
	<span>&nbsp;</span> Menu
</button>

<div class="nav">
	<div class="sw">
		
		<nav>
			<div class="sw full">
				<ul>
					<li><a href="#" class="button">Now Selling</a></li>
					<li><a href="#">Goose Pond Properties</a></li>
					<li><a href="#">Other Projects</a></li>
					<li><a href="#">Services</a></li>
					<li><a href="#">The Latest</a></li>
				</ul>
			</div><!-- .sw -->
		</nav>
	
		<div class="nav-top">
		
			<form action="/" method="get" class="single-form search-form">
				<fieldset>
					<input type="text" name="s" placeholder="Search Goose Pond Properties...">
					<span class="close t-fa-abs fa-close">Close</span>
					<button class="t-fa-abs fa-search">Search</button>
				</fieldset>
			</form>
		
			<a href="#">Who We Are</a>
			<a href="#">FAQ</a>
			<a href="#">Contact</a>
			
			<?php include('i-social.php'); ?>
			
		</div><!-- .nav-top -->
	</div><!-- .sw -->
</div><!-- .nav -->