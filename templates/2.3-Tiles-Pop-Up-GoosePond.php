<?php $bodyclass = ''; ?>
<?php include('inc/i-header.php'); ?>

<div class="hero fader-wrap">

	<div class="big-fader fader">
		<div class="fader-item" >
		
			<div class="gmap lot-map" data-lots="inc/lot-information.php">
				<!-- 
					The data-center and data-zoom attributes are required for a map to be instantiated on this element
					In the case of the lot map, the map will fit all of the lots specified by the content of data-lots on load
				-->
				<div class="map" data-center="47.4137545,-53.5238866" data-zoom="15">
				</div><!-- .map -->
			</div><!-- .hero-map -->

			
		</div><!-- .fader-item -->
		
	</div><!-- .fader -->
	
	<div class="hero-caption dark-bg sw full">
		<div class="hero-caption-nav">
		
			<div class="selector with-arrow">
				<select>
					<option>Lot Plan</option>
					<option>Location Map</option>
					<option>Select Lot &amp; Price</option>
				</select>
				<span class="value">&nbsp;</span>
			</div><!-- .selector -->
		
			<div class="hero-caption-nav-item">
				<button class="button">Lot Plan</button>
			</div><!-- .hero-caption-nav-item -->
			<div class="hero-caption-nav-item">
				<button class="button selected">Location Map</button>
			</div><!-- .hero-caption-nav-item -->
			<div class="hero-caption-nav-item">
				<button class="button">Select Lot &amp; Price</button>
			</div><!-- .hero-caption-nav-item -->
		</div><!-- .hero-caption-nav -->
	</div><!-- .hero-caption -->
	
	
</div><!-- .hero -->

<div class="body">
	
	<section>
		<div class="sw">
		
			<div class="hgroup">
				<h1 class="title">Other Phases</h1>
				<span class="subtitle">Upcoming Developments</span>
			</div>
		
			<div class="swiper-wrapper phase-swiper button-dots">
				<div class="swiper" data-dots="true" data-arrows="true" data-slides-to-show="3" data-responsive='<?php echo json_encode(array((array('breakpoint'=>950,'settings'=>array('slidesToShow'=>2))),array('breakpoint'=>650,'settings'=>'unslick'))); ?>'>
				
					<div class="swipe-item">
						<a href="#" class="phase-item lazybg" data-src="http://maps.google.com/maps/api/staticmap?size=180x180&center=47.4354943,-53.5163121&sensor=false&zoom=14">
							<div>
								<span class="title">Phase Two</span>
								<span class="button darken">More Details</span>
							</div>
						</a><!-- .phase-item -->						
					</div><!-- .swipe-item -->
						
					<div class="swipe-item">
						<a href="#" class="phase-item lazybg" data-src="http://maps.google.com/maps/api/staticmap?size=180x180&center=47.4354943,-53.5163121&sensor=false&zoom=14">
							<div>
								<span class="title">Phase Three</span>
								<span class="button darken">More Details</span>
							</div>
						</a><!-- .phase-item -->
					</div><!-- .swipe-item -->
					
					<div class="swipe-item">	
						<a href="#" class="phase-item lazybg" data-src="http://maps.google.com/maps/api/staticmap?size=180x180&center=47.4354943,-53.5163121&sensor=false&zoom=14">
							<div>
								<span class="title">Phase Four</span>
								<span class="button darken">More Details</span>
							</div>
						</a><!-- .phase-item -->
					</div><!-- .swipe-item -->
					
					<div class="swipe-item">	
						<a href="#" class="phase-item lazybg" data-src="http://maps.google.com/maps/api/staticmap?size=180x180&center=47.4354943,-53.5163121&sensor=false&zoom=14">
							<div>
								<span class="title">Phase Five</span>
								<span class="button darken">More Details</span>
							</div>
						</a><!-- .phase-item -->
					</div><!-- .swipe-item -->
					
				</div><!-- .swiper -->
			</div><!-- .swiper-wrapper -->
		
		</div><!-- .sw -->
	</section>
	
</div><!-- .body -->


<?php include('inc/i-footer.php'); ?>