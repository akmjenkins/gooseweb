;(function(context) {

	var debounce = function() {
		this.isProcessing = false;
		this.method = null;
	};
		
	debounce.prototype.requestProcess = function(method,throttle) {
		if(!this.isProcessing) {
			this.method = method;
			this.isProcessing = true;
			
			var cb = function() {
				this.method();
				this.method = null;
				this.isProcessing = false;
			}.bind(this);;
			
			
			if(throttle) {
				setTimeout(cb,throttle);
			} else {
				requestAnimationFrame(cb);
			}
			
		}
	};

	//CommonJS
	if(typeof module !== 'undefined' && module.exports) {
		module.exports = function() { 
			return new debounce(); 
		};
	//CodeKit
	} else if(context) {
		context.debounce = function() {
			return new debounce();
		}
	}
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));