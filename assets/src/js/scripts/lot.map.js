;(function(context) {

	var gmap;
	var customInfoWindow;
	var tim;
	
	if(context) {
		gmap = context.gmap;
		customInfoWindow = context.CustomInfoWindow;
		tim = context.tim;
	} else {
		gmap = require('./gmap.js');
		customInfoWindow = require('./map/custom.infowindow.js');
		tim = require('./tim.microtemplate.js');
	}
	
	var INFO_WINDOW_TEMPLATE = '\
		<span class="t-fa-abs fa-close">Close</span>\
		<div class="lot-info">\
			<div class="lot-info-img has-img-{{image.length}}" style="background-image: url({{image}});">&nbsp;</div>\
			<div class="lot-info-content">\
				<span class="title">{{title}} <span class="status {{status}}">{{status}}</span></span>\
				<span class="price {{status}}">{{price_string}}</span>\
				<p>{{excerpt}}</p>\
				<a href="{{link}}" class="button on-light" rel="external">More Info</a>\
			</div>\
		</div>\
	';
	
	var
		map,
		formatMoney = function (str,c,d,t) {
			var n = str,
			c = isNaN(c = Math.abs(c)) ? 2 : c, 
			d = d == undefined ? "." : d, 
			t = t == undefined ? "," : t, 
			s = n < 0 ? "-" : "", 
			i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", 
			j = (j = i.length) > 3 ? j % 3 : 0;
			return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
		}
		LOTS_AJAX_URL = templateJS.templateURL + '/inc/lots.php',
		PHASES_AJAX_URL = templateJS.templateURL + '/inc/phases.php',
		MARKER_IMAGE_URL = templateJS.templateURL + '/inc/image.php',
		MIN_LOT_LABEL_ZOOM_LEVEL = 18,
		MIN_PHASE_LABEL_ZOOM_LEVEL = 12,
		SHOW_CLASS = 'show-map',
		LOADED_CLASS = 'map-is-loaded',
		LOT_COLOR_AVAILABLE = '#2eac24',
		LOT_COLOR_SOLD = '#ce3030',
		$phaseSelector = $('#phase-selector'),
		$overlayEl = $('.gooseweb-map-overlay'),
		$mapEl = $('.gooseweb-map'),
		$body = $('body'),
		$mobileInfoWindow = $('.mobile-infowindow'),
		PHASE_POLYGON_OPTIONS = {
			strokeColor: '#0000000',
			strokeOpacity:0.2,
			strokeWeight:1,
			//fillColor: phase.color,
			fillOpacity: 0.6,
			//map:this.map,
			//path:phase.path,
		},
		LOT_POLYGON_OPTIONS = {
			//path:lot.path,
			//map:this.map,
			strokeColor: '#0000000',
			strokeOpacity:0.2,
			fillColor: '#000000',
			fillOpacity:0.2,
			strokeWeight:1
			//title:num,
		},
		lotMap = {
			
			phasesReq:null,
			lotsReqs:{},
			
			phases: [],
			lots:{},
			
			createMap: function() {
				var self = this;
				if(map) { return; }
				
				var dfd = $.Deferred();
				
				gmap.buildMap($mapEl,{
					zoom: 1,
					scrollwheel:true,
					center: {lat:52,lng:52}
				});
				
				$mapEl.on('mapLoad',function(e,map) { dfd.resolve(map);  });
				$mapEl.on('mapZoomed',function(e) {
					if(!map) { return; }
					
					var zoom = self.getMap().getZoom();
					
					$.each(self.lots,function(i,lotObject) { 
						if(!self.shouldShowLotLabel()) {
							lotObject.marker.setMap(null); 
						} else if(lotObject.polygon.getMap()) {
							lotObject.marker.setMap(self.getMap());
						}
						
					});
					
					$.each(self.phases,function(i,phaseObject) { 
					
						if(!self.shouldShowPhaseLabel()) {
							phaseObject.marker.setMap(null); 
						} else if(phaseObject.polygon.getMap()) {
							phaseObject.marker.setMap(self.getMap());
						}
						
					});
					
				});
				
				return dfd
					.pipe(function(theMap) {
						map = theMap;
						self.customInfoWindow = customInfoWindow({},self.getMap());
						return self.getPhases();
					})
					.pipe(function(phases) {
						//zoom to show all phases
						self.showAllPhases();
						$.each(phases,function(i,phase) { $phaseSelector.append('<option value="'+phase.title+'">'+phase.title+'</option>'); });
						$overlayEl.addClass(LOADED_CLASS); 
					})
			},
			
			getCentroidOfPoints: function(points) {
				  var centroid = {x: 0, y: 0};
				  for(var i = 0; i < points.length; i++) {
					 var point = points[i];
					 centroid.x += point.x;
					 centroid.y += point.y;
				  }
				  centroid.x /= points.length;
				  centroid.y /= points.length;
				  return centroid;
			},
			
			getCentroidOfPolygon: function(poly) {
				var pts = [],proj = this.getMap().getProjection();
				poly.getPath().forEach(function(p,i) {
					pts.push(proj.fromLatLngToPoint(p));
				});
				return proj.fromPointToLatLng(this.getCentroidOfPoints(pts));
			},
			
			processPhaseObject: function(phaseObject) {
				var self = this;
				phaseObject.polygon = new google.maps.Polygon($.extend({path:phaseObject.path,fillColor:phaseObject.color},PHASE_POLYGON_OPTIONS));
				phaseObject.bounds = this.getBoundsForPolygon(phaseObject.polygon);
				phaseObject.marker = new google.maps.Marker({
					position:this.getCentroidOfPolygon(phaseObject.polygon),
					icon:MARKER_IMAGE_URL + '?t='+phaseObject.title
				});
				
				//listeners
				google.maps.event.addListener(phaseObject.polygon,'click',function() { self.selectPhase(phaseObject); });
				google.maps.event.addListener(phaseObject.marker,'click',function() { self.selectPhase(phaseObject); });
				google.maps.event.addListener(phaseObject.marker,'mouseover',function() { phaseObject.polygon.setOptions({fillOpacity:PHASE_POLYGON_OPTIONS.fillOpacity+0.4}); });
				google.maps.event.addListener(phaseObject.marker,'mouseout',function() { phaseObject.polygon.setOptions({fillOpacity:PHASE_POLYGON_OPTIONS.fillOpacity}); });
				google.maps.event.addListener(phaseObject.polygon,'mouseover',function() { phaseObject.polygon.setOptions({fillOpacity:PHASE_POLYGON_OPTIONS.fillOpacity+0.4}); });
				google.maps.event.addListener(phaseObject.polygon,'mouseout',function() { phaseObject.polygon.setOptions({fillOpacity:PHASE_POLYGON_OPTIONS.fillOpacity}); });
				
				this.phases.push(phaseObject);
				return phaseObject;
			},
			
			processLotObject: function(lotObject) {
				var self = this;
				lotObject.polygon = new google.maps.Polygon($.extend({path:lotObject.path,fillColor:lotObject.color},LOT_POLYGON_OPTIONS));
				if(this.isLotSold(lotObject)) {
					lotObject.polygon.setOptions({fillColor:LOT_COLOR_SOLD});
				} else if(this.isLotAvailable(lotObject)) {
					lotObject.polygon.setOptions({fillColor:LOT_COLOR_AVAILABLE});
				}
				lotObject.bounds = this.getBoundsForPolygon(lotObject.polygon);
				lotObject.price_string = '$'+formatMoney(lotObject.price,0);
				lotObject.marker = new google.maps.Marker({
					position:this.getCentroidOfPolygon(lotObject.polygon),
					icon:MARKER_IMAGE_URL + '?t='+lotObject.title
				});

				//listeners
				google.maps.event.addListener(lotObject.marker,'mouseover',function() { lotObject.polygon.setOptions({fillOpacity:LOT_POLYGON_OPTIONS.fillOpacity+0.4}); });
				google.maps.event.addListener(lotObject.marker,'mouseout',function() { lotObject.polygon.setOptions({fillOpacity:LOT_POLYGON_OPTIONS.fillOpacity}); });				
				google.maps.event.addListener(lotObject.polygon,'mouseover',function() { lotObject.polygon.setOptions({fillOpacity:LOT_POLYGON_OPTIONS.fillOpacity+0.4}); });
				google.maps.event.addListener(lotObject.polygon,'mouseout',function() { lotObject.polygon.setOptions({fillOpacity:LOT_POLYGON_OPTIONS.fillOpacity}); });
				google.maps.event.addListener(lotObject.marker,'click',function(e) { self.showInfoWindow(lotObject,e.latLng); });
				google.maps.event.addListener(lotObject.polygon,'click',function(e) { self.showInfoWindow(lotObject,e.latLng); });
				
				this.lots[lotObject.title] = lotObject;
				return lotObject;
			},
			
			isLotSold: function(lotObject) {
				return lotObject.status === 'sold';
			},
			
			isLotAvailable: function(lotObject) {
				return lotObject.status === 'available';
			},
			
			showAllPhases: function() {
				var self = this;
				$.each(this.lots,function(i,lotObject) { self.hideMapObject(lotObject); });
				$.each(this.phases,function(i,phaseObject) { self.showMapObject(phaseObject); });
				this.fitMapToPhases(this.phases);
			},
			
			showInfoWindow: function(lotObject,latLng) {
				this.customInfoWindow.setContent(tim(INFO_WINDOW_TEMPLATE,lotObject));
				this.customInfoWindow.open(latLng);
				
				$mobileInfoWindow.html(tim(INFO_WINDOW_TEMPLATE,lotObject));
				$mobileInfoWindow.addClass('show');
			},
			
			hideInfoWindow: function() {
				this.customInfoWindow.hide();
				$mobileInfoWindow.removeClass('show');
			},
			
			selectPhase: function(phaseObject) {
				var self = this;
				this.fitMapToPhases([phaseObject]);
				$.each(this.phases,function(i,phaseObject) { self.hideMapObject(phaseObject); });
				$.each(this.lots,function(i,lotObject) { self.hideMapObject(lotObject); });
				
				this
					.getLotsForPhase(phaseObject)
					.pipe(function(lots) {
						$.each(lots,function(i,lotObject) { self.showMapObject(lotObject); });
					});
				
			},
			
			getBoundsForPolygon: function(poly) {
				var b = new google.maps.LatLngBounds();
				poly.getPath().forEach(function(p,i) { b.extend(p); });				
				return b;
			},
			
			toggleMapObject: function(mapObject) {
				mapObject.polygon.getMap() ? this.hideMapObject(mapObject) : this.showMapObject(mapObject);
			},
			
			shouldShowLotLabel: function() {
				return this.getMap().getZoom() >= MIN_LOT_LABEL_ZOOM_LEVEL
			},
			
			shouldShowPhaseLabel: function() {
				return this.getMap().getZoom() >= MIN_PHASE_LABEL_ZOOM_LEVEL
			},
			
			showMapObject: function(mapObject) {
				mapObject.polygon.setMap(this.getMap());
				
				//if this has a status - it's a lot
				if(mapObject.status && this.shouldShowLotLabel()) {
					mapObject.marker.setMap(this.getMap());
				} else if(this.shouldShowPhaseLabel()) {
					mapObject.marker.setMap(this.getMap());	
				}
			},
			
			hideMapObject: function(mapObject) {
				mapObject.polygon.setMap(null);
				mapObject.marker.setMap(null);
			},
			
			fitMapToPhases: function(phaseObjects) {
				var b = new google.maps.LatLngBounds();
				$.each(phaseObjects,function(i,phase) { b.union(phase.bounds); });
				this.getMap().fitBounds(b);
			},
			
			getMap: function() {
				return map ? map.map : null;
			},
			
			show: function(phaseTitle) {
				var self = this;
				$body.addClass(SHOW_CLASS);
				if(!this.hasLoaded()) {
					this
						.createMap()
						.pipe(function() {
							$.each(self.phases,function(i,phaseObject) {
								if(phaseObject.title === phaseTitle) {
									self.selectPhase(phaseObject);
									return false;
								}
							});
						})
						
				}
			},
			
			toggle: function(phaseTitle) {
				if(this.isVisible()) {
					this.hide();
				} else {
					this.show(phaseTitle);
				}
			},
			
			hide: function() {
				$body.removeClass(SHOW_CLASS);
			},
			
			isVisible: function() {
				return $body.hasClass(SHOW_CLASS);
			},
			
			hasLoaded: function() {
				return $overlayEl.hasClass(LOADED_CLASS);
			},
			
			getPhases: function() {
				var self = this;
				if(!this.phasesReq) {
					this.phasesReq = $.ajax({url:PHASES_AJAX_URL,dataType:'json'})
					.pipe(function(r,status,jqXHR) { 
						$.each(r,function(i,phaseObject) { self.processPhaseObject(phaseObject); });
						return $.Deferred().resolve(r); 
					});
				}
				
				return this.phasesReq;
			},
			
			getLotsForPhase: function(phaseObject) {
				var self = this;
				if(!this.lotsReqs[phaseObject.title]) {
					this.lotsReqs[phaseObject.title] = $.ajax({url:LOTS_AJAX_URL,dataType:'json',data:{lots:phaseObject.lots}})
						.pipe(function(r,status,jqXHR) { 
							$.each(r,function(i,lotObject) { self.processLotObject(lotObject); });
							return $.Deferred().resolve(r); 
						});
				}
				
				return this.lotsReqs[phaseObject.title]
			}
		};
		
		$(document)
			.on('click','.custom-info-window-content .fa-close',function() { lotMap.hideInfoWindow(); })
			.on('click','.toggle-map-overlay',function() { 
				lotMap.toggle($(this).data('phase')); 
			});
			
		$phaseSelector
			.on('change',function() {
				var val = $(this).val();
				
				if(!val) { 
					lotMap.showAllPhases();
					return;
				}
				
				var selectedPhase;
				$.each(lotMap.phases,function(i,phase) {
					if(phase.title === val) {
						selectedPhase = phase;
						return false;
					}
				});
				
				selectedPhase && lotMap.selectPhase(selectedPhase);
			});
			
		window.lotMap = lotMap;

}(typeof ns !== 'undefined' ? window[ns] : undefined));