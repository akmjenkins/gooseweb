;(function(context) {
	
	var HTMLMarker,tests,debounce;
	
	if(context) {
		debounce = context.debounce;
		HTMLMarker = context.HTMLMarker;
		tests = context.tests;
	} else {
		HTMLMarker = require('./map/html.marker.js');
		tests = require('./tests.js');
		debounce = require('./debounce.js');
	}
	
	//Simple Embedded Google Map

	var loadGMap = function(cb) {	
		google.load('maps',3.19, {
			"base_domain":"google.ca","other_params":"sensor=false",
			"callback": cb
		});
	};

	var loadGoogleLoader = function(cb) {
		$.getScript('https://www.google.com/jsapi').done(cb);
	};
	
	var Map = function(containerEl,options) {
		var self = this;
		
		google.maps.visualRefresh = true;
		
		this.canvas = containerEl;
		this.markers = [];
		var center = containerEl.data('center').split(',');
		
		this.canvas.on('mapLoad',function(e) { 
			containerEl.addClass('loaded'); 
			
			//fit to bounds on markers if necessary
			if(self.markers.length) {
				var 
					mapBounds = self.map.getBounds(),
					markerBounds = new google.maps.LatLngBounds();
					
				$.each(self.markers,function(i,marker) {
					markerBounds.extend(marker.getPosition());
				});
				
				if(!mapBounds.union(markerBounds).equals(mapBounds)) {
					self.map.fitBounds(markerBounds);
				}
				
			}
			
			
		});
		
		this.mapOptions = $.extend({
			center: new google.maps.LatLng(+center[0], +center[1]),
			zoom: +containerEl.data('zoom') || 15,
			streetViewControl:false,
			panControl:false,
			scrollwheel: false,
			draggable: (!tests.touch() || $(window).outerWidth() > 600),
			mapTypeControlOptions:{
				style:google.maps.MapTypeControlStyle.DROPDOWN_MENU
			},
			zoomControlOptions:{
				style:google.maps.ZoomControlStyle.SMALL
			},
			mapTypeId: google.maps.MapTypeId.ROADMAP
		},options || {});
		
		this.map = new google.maps.Map(this.canvas[0],this.mapOptions);
		
		var 
			lastMarkerImage,
			lastMarkerIcon,
			markers = containerEl.data('markers');
		markers && markers.length && $.each(markers,function(i,markerObj) {
			var 
				markerIcon = null,
				center = markerObj.position.split(','),
				createMarker = function() {
					var pos = markerObj.position.split(',');
					var opts = {
						position: new google.maps.LatLng(+pos[0], +pos[1]),
						map: self.map,
						title: markerObj.title
					};
					
					if(markerObj.htmlmarker) {
						self.markers.push(HTMLMarker($.extend(opts,{"class":markerObj.htmlmarker})));
					} else {
						self.markers.push(new google.maps.Marker($.extend(opts,{icon:markerIcon})));
					}
				};
			
			if(markerObj.image) {
				if(lastMarkerImage === markerObj.image) {
					markerIcon = lastMarkerIcon
					return;
				}
				
				$('<img/>')
					.on('load',function() {
						var
							width = this.width,
							height = this.height,
							anchor = markerObj.anchor || {x:width/2,y:height/2};
							
						//anchor is center
						markerIcon = { 
							anchor: new google.maps.Point(anchor.x,anchor.y), 
							url: templateJS.templateURL+markerObj.image
						};
						
						lastMarkerImage = markerObj.image;
						lastMarkerIcon = markerIcon;
						createMarker();
					})
					.attr('src',templateJS.templateURL+markerObj.image);
					return;
			}
			
			createMarker();

		});
		
		this.convertListeners();
		
	};

	
	Map.prototype.convertListeners = function() {
		this.resizeDebounce = debounce();
	
		this.mapListeners = {};
	
			var 
				self = this,
				ns = google.maps.event,
				loadListener = ns.addListener(self.map,'tilesloaded',function() { 
					ns.removeListener(loadListener);
					self.canvas.trigger('mapLoad',[self]);
					self.mapListeners.resizeListener = self.mapListeners.resizeListener || ns.addListener(self.map,'resize',function() { self.canvas.trigger('mapResized'); });
					self.mapListeners.idleListener = self.mapListeners.idleListener || ns.addListener(self.map,'idle',function() { 
							var 
								args = {},
								center = self.map.getCenter();
								
								args.lat = center.lat();
								args.lng = center.lng();
								args.zoom = self.map.getZoom();
							
							self.canvas.trigger('mapMoved',[args]); 
					});
					self.mapListeners.zoomListener = self.mapListeners.zoomListener || ns.addListener(self.map,'zoom_changed',function() { self.canvas.trigger('mapZoomed',[self.map.getZoom()]); });
				});
				self.mapListeners.mapTypeListener = self.mapListeners.mapTypeListener || ns.addListener(self.map,'maptypeid_changed',function() { self.canvas.trigger('mapTypeChanged'); });
				
			self.canvas.on('doResizeMap',function() { ns.trigger(self.map,'resize'); });
			
		$(window).on('resize',function() {
			self.resizeDebounce.requestProcess(function() {
				self.canvas.trigger('doResizeMap')
			},250);
		});
	
	};

	var 
		hasRequestedGoogle = false,
		googleDefer = $.Deferred(),
		googleRequestComplete = googleDefer.promise(),
		buildMap = function(options) {
			var el = this;
			
			var cb = function(options) {
				var map = new Map($(this),options);
				window.maps = window.maps || [];
				window.maps.push(map);
				
				$(this).data('map',map);
			}
			
			if(!hasRequestedGoogle) {
				hasRequestedGoogle = true;
			} else {
				return googleRequestComplete.done(function() {
					cb.apply(el,[options]);
				});
			}
			
			if(window.google === undefined || window.google.maps === undefined) { 
			
				window.createMap = function() { 
					googleDefer.resolve();
					cb.apply(el,[options]); 
				};
				
				return $.getScript('https://maps.googleapis.com/maps/api/js?sensor=false&callback=createMap');
			}
			
			if(window.google === undefined) { return loadGoogleLoader(function() { loadGMap(function() { cb.apply(el,[options]); }); }); }
			
			if(window.google.maps === undefined) { return loadGMap(function() { cb.apply(el,[options]); }); }
			
			cb.apply(el,[options]);
		};
	
	$('div.map').each(function() { buildMap.apply($(this)); });	
	
	//CommonJS
	if(typeof module !== 'undefined' && module.exports) {
		module.exports = {
			buildMap: function(el,options) { return buildMap.apply(el,[options]); }
		}
	//CodeKit
	} else if(context) {
		context.gmap = function(el,options) { return buildMap.apply(el,[options]); }
	}	
	
}(typeof ns !== 'undefined' ? window[ns] : undefined));